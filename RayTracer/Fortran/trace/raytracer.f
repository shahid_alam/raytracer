	module RAYTRACER_

	use VECTOR_
	use RGBCOLOR_
	use RAY_
	use SPHERE_
	use PLANE_
	use LIGHT_
	use MATERIAL_
	use SCENE_

	/**
	 * @section DESCRIPTION
	 *
	 * The RayTracer Class.
	 * Provides the RayTracer class ....
	 *
	 */
	type, public :: RayTracer
		private
			type(Scene) :: scene_t

		contains
			procedure, private :: refract
			procedure, private :: reflect
			procedure, private :: reflectance
			procedure, private :: transmission

			generic :: init => init_rt
			procedure :: init_rt
			procedure :: RayTrace
	end type RayTracer

	contains

	subroutine init_rt(R, s)
		class(RayTracer), intent(inout) :: R
		type(Scene), intent(in) :: s
		R%scene_t = s
	end subroutine init_rt

	! ------------------------------------------------------------------------------------------
	!
	! Check all the objects (spheres and planes) in the scene
	!
	! ------------------------------------------------------------------------------------------
	RECURSIVE function RayTrace(R, ray_t, color, t, coef, depth) RESULT(RES)
		class(RayTracer), intent(inout) :: R
		type(Ray), intent(inout) :: ray_t
		type(RGBColor), intent(inout) :: color
		double precision, intent(inout) :: t
		double precision, intent(in) :: coef
		integer, intent(in) :: depth

		integer :: i, n, HIT
		type(Ray) :: reflectedRay, lightRay, refractedRay
		type(Sphere) :: sphere_t
		type(Plane) :: plane_t
		type(Light) :: light_t
		type(Material) :: material_t
		type(Vector) :: dir
		type(RGBColor) :: clr, transparency
		logical :: inShadow, RES
		double precision :: dot, lambert, specular, reflection, refraction, t1, refractionIn, refractionOut, prevDist, refl
		HIT = 0

		do i = 1, R%scene_t%numberOfPlanes
			plane_t = R%scene_t%plane_t(i)
			if (plane_t%Hit(ray_t, t)) then
				material_t = plane_t%M
				HIT = 1
			end if
		end do
		do i = 1, R%scene_t%numberOfSpheres
			sphere_t = R%scene_t%sphere_t(i)
			if (sphere_t%Hit(ray_t, t)) then
				material_t = sphere_t%M
				HIT = 1
			end if
		end do

		! No object is hit
		if (HIT .EQ. 0) then
			RES = .FALSE.
			return
		end if

#ifdef __TESTING__
		color = material_t%GetColor()
		RES = .TRUE.
		return
#endif

		!
		! Computing reflection for next iteration
		! Will also be used in specualr shading
		!
		RES = R%reflect(ray_t%hitInfo_t%Normal, ray_t%dir)
		call reflectedRay%init(ray_t%hitInfo_t%Position + ray_t%dir * EPSILON, ray_t%dir)
		dot = reflectedRay%dir * ray_t%dir

		!
		! Diffuse shading --> Dot product of light ray and normal at the hit/intersection point
		!
		do i = 1, R%scene_t%numberOfLights
			light_t = R%scene_t%light_t(i)
			! Diffuse shading
			dir = light_t%GetPosition() - ray_t%hitInfo_t%Position
			! Check if ray and light rays are in opposite direction
			if (ray_t%hitInfo_t%Normal * dir .LE. 0.0) then
				goto 1001
			end if
			! We need distance to the light (t), so normalize the Vector 'dir' manually
			t = sqrt(dir * dir)
			if (t .LE. 0.0) then
				goto 1001
			end if
			dir = dir / t
			call lightRay%init(ray_t%hitInfo_t%Position, dir)
			inShadow = .FALSE.
			! computation of the shadows
			do n = 1, R%scene_t%numberOfPlanes
				plane_t = R%scene_t%plane_t(n)
				if (plane_t%Hit(lightRay, t)) then
					inShadow = .TRUE.
					goto 1003
				end if
			end do
			do n = 1, R%scene_t%numberOfSpheres
				sphere_t = R%scene_t%sphere_t(n)
				if (sphere_t%Hit(lightRay, t)) then
					inShadow = .TRUE.
					goto 1003
				end if
			end do

1003		if (.NOT. inShadow) then
				! Lambert calculation
				lambert = (lightRay%dir * ray_t%hitInfo_t%Normal) * coef
				color = color + light_t%GetColor() * material_t%GetColor() * lambert
				! Compute and add specular shading to color
				if (dot .GT. 0.0) then
					specular = (dot ** 20) * (1.0 - coef)                  ! pow (dot, 20) = dot ** 20
					color = color + light_t%GetColor() * specular
				end if
			end if
1001	continue
		end do

		! Iterate on the next reflection (computed above for specular shading)
		! with a depth level of "depth"
		reflection = material_t%GetReflection()
		if (reflection .GT. 0.0 .AND. depth .GT. 0) then
			t1 = 10000.0
			clr = R%scene_t%GetBGColor()
			RES = R%RayTrace(reflectedRay, clr, t1, (coef * reflection), depth-1)
			color = color + clr * reflection
		end if

		!
		! Iterate on the next refraction with a depth level of "depth"
		!
		if (material_t%GetTransparency() .GT. 0.0 .AND. depth .GT. 0) then
			refractionIn = REFRACTION_IN
			refractionOut = REFRACTION_OUT
			refraction = refractionIn / refractionOut
			prevDist = ray_t%dir%z
			ray_t%dir%z = DISTANCE
			!
			! Total internal reflection
			!
			if (R%refract(ray_t%hitInfo_t%Normal, ray_t%dir, refractionIn, refractionOut)) then
				t1 = 10000.0
				clr = R%scene_t%GetBGColor()
				call refractedRay%init(ray_t%hitInfo_t%Position + ray_t%dir * EPSILON, ray_t%dir)
				if (R%RayTrace(refractedRay, clr, t1, (coef * refraction), depth-1)) then
					RES = R%RayTrace(refractedRay, clr, t1, (coef * refraction), depth-1)
					material_t = refractedRay%hitInfo_t%material_t
				end if
				transparency = R%transmission(material_t%GetColor(), coef, refractedRay%hitInfo_t%Distance)
				transparency = transparency * material_t%GetTransparency()
				if (clr%red .GT. 1.0 .OR. clr%green .GT. 1.0 .OR. clr%blue .GT. 1.0) then
					clr = R%scene_t%GetBGColor()
				end if
				color = color + clr * transparency * (coef * 0.5)
			!
			! No total internal reflection
			!
			else
				t1 = 10000.0
				clr = R%scene_t%GetBGColor()
				call refractedRay%init(ray_t%hitInfo_t%Position + ray_t%dir * EPSILON, ray_t%dir)
				if (R%RayTrace(refractedRay, clr, t1, (coef * refraction), depth-1)) then
					RES = R%RayTrace(refractedRay, clr, t1, (coef * refraction), depth-1)
					material_t = refractedRay%hitInfo_t%material_t
				end if
				refl = R%reflectance(refractedRay%hitInfo_t%Normal, refractedRay%dir, refractionIn, refractionOut)
				refl = refl * material_t%GetTransparency()
				if (clr%red > 1.0 .OR. clr%green > 1.0 .OR. clr%blue > 1.0) then
					clr = R%scene_t%GetBGColor()
				end if
				color = color + clr * (refl * coef * 0.5)
			end if
			ray_t%dir%z = prevDist
		end if

		RES = .TRUE.
		return
	end function RayTrace


	! ------------------------------------------------------------------------------------------
	!
	! Refraction equation:
	!
	! r = (n1/n2) * d + [ (n1/n2) * cos(D) - sqrt(1 - sqr(sin(T)) ] * n
	! where
	!    cos(D) = -(d * n)
	!    sqr(sin(T)) = sqr(n1/n2) * [ 1 - sqr(cos(D)) ]
	!
	! In case of refraction, there�s a condition that limits the range of incoming angles T.
	! Outside this range, the refracted direction vector does not exist. Hence, there�s no
	! transmission. This is called total internal reflection. The condition is:
	! sin(T) > 1.0
	!
	! ------------------------------------------------------------------------------------------
	logical function refract(R, normal, dir, n1, n2)
		class(RayTracer), intent(inout) :: R
		class(Vector), intent(inout) :: normal, dir
		double precision, intent(in) :: n1, n2
		double precision :: n, cosD, sinT, cosT
		
		n = n1 / n2
		cosD = -(normal * dir)
		sinT = n * n * (1.0 - cosD * cosD)

		if (sinT .GT. 1.0) then
			refract = .FALSE.
			return
		end if

		cosT = sqrt(1.0 - sinT)
		! Fill the new dir vector
		dir = (dir * n) + (normal * (n * cosD - cosT))
		refract = .TRUE.
		return
	end function refract

	! ------------------------------------------------------------------------------------------
	!
	! Reflection equation:
	!
	! r = d + 2 * [cos(D)] * n
	! where cos(D) = -(d * n)
	!
	! ------------------------------------------------------------------------------------------
	logical function reflect(R, normal, dir)
		class(RayTracer), intent(inout) :: R
		type(Vector), intent(inout) :: normal, dir
		double precision :: cosD

		cosD = -(normal * dir)
		! Fill the new dir vector
		dir = dir + normal * cosD * 2.0
		reflect = .TRUE.
		return
	end function reflect

	! ------------------------------------------------------------------------------------------
	!
	! Applying shlick's approximation for reflectance
	!
	! ------------------------------------------------------------------------------------------
	double precision function reflectance(R, normal, dir, n1, n2)
		class(RayTracer), intent(inout) :: R
		type(Vector), intent(inout) :: normal, dir
		double precision, intent(in) :: n1, n2
		double precision :: re, n, x, cosD, sinT

		reflectance = 1.0
		re = (n1 - n2) / (n1 + n2)
		re = re * re
		cosD = -(normal * dir)
		if (n1 .GT. n2) then
			n = n1 / n2
			sinT = n * n * (1.0 - cosD * cosD)
			if (sinT .GT. 1.0) then
				return
			end if
			cosD = sqrt(1.0 - sinT)
		end if
		x = 1.0 - cosD
		reflectance = re + (1.0 - re) * x * x * x * x * x

		return
	end function reflectance

	! ------------------------------------------------------------------------------------------
	!
	! Aplying Beer�Lambert law
	! How much light is absorbed while traveling through a material.
	! The law states that there is a logarithmic dependence between the transmission
	! (or transmissivity), T, of light through a substance and the product of the
	! absorption coefficient of the substance, and the distance the light travels
	! through the material (i.e. the path length).
	! Equation is:
	! color out = color in ( exp (absorbance) )
	! where
	! absorbance = color * coefficient * path length of the ray
	!
	! ------------------------------------------------------------------------------------------
	type(RGBColor) function transmission(R, clr, coef, dist)
		class(RayTracer), intent(inout) :: R
		type(RGBColor), intent(in) :: clr
		double precision, intent(in) :: coef, dist
		type(RGBColor) :: absorbance

		absorbance = clr * coef * (-dist)
		transmission = RGBColor ( exp(absorbance%red), exp(absorbance%green), exp(absorbance%blue) )
		return
	end function transmission

	end module RAYTRACER_
