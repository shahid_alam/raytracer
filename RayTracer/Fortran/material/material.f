	module MATERIAL_

	use RGBCOLOR_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The Material Class.
	! Provides the Material class ....
	!
	!
	type, public :: Material
		private
			double precision :: gloss = 2.0
			double precision :: transparency = 0.0
			double precision :: reflection = 0.0
			double precision :: refractionIn = 0.0
			double precision :: refractionOut = 0.0
			type(RGBColor) :: color = RGBColor(0.0, 0.0, 0.0)

		contains
			! specifies the Gloss (or shininess) of the element
			! value must be between 1 (very shiney) and 5 (matt) for a realistic effect 
			procedure :: GetGloss
			procedure :: SetGloss

			! defines the transparency of the element. 
			! values must be between 0 (opaque) and 1 (fully transparent);
			procedure :: GetTransparency
			procedure :: SetTransparency

			! specifies how much light the element will reflect
			! value must be between 0 (no reflection) to 1 (total reflection/mirror)
			procedure :: GetReflection
			procedure :: SetReflection

			! refraction index
			! specifies how the material will bend the light rays
			! value must be between <0,1] (total reflection/mirror)
			procedure :: GetRefractionIn
			procedure :: SetRefractionIn
			procedure :: GetRefractionOut
			procedure :: SetRefractionOut

			! indicates that the material has a texture and therefore the exact
			! u,v coordinates are to be calculated by the element
			! and passed on in the GetColor function
			procedure :: HasTexture

			! retrieves the actual color of the material
			procedure :: GetColor=>GetColor_MATERIAL
			procedure :: SetColor=>SetColor_MATERIAL
	end type Material

	contains

	double precision function GetGloss(M)
		class(Material), intent(in) :: M
		GetGloss = M%gloss
	end function GetGloss

	subroutine SetGloss(M, value)
		class(Material), intent(inout) :: M
		double precision, intent(in) :: value
		M%gloss = value
	end subroutine SetGloss

	double precision function GetTransparency(M)
		class(Material), intent(in) :: M
		GetTransparency = M%transparency
	end function GetTransparency

	subroutine SetTransparency(M, value)
		class(Material), intent(inout) :: M
		double precision, intent(in) :: value
		M%transparency = value
	end subroutine SetTransparency

	double precision function GetReflection(M)
		class(Material), intent(in) :: M
		GetReflection = M%reflection
	end function GetReflection

	subroutine SetReflection(M, value)
		class(Material), intent(inout) :: M
		double precision, intent(in) :: value
		M%reflection = value
	end subroutine SetReflection

	double precision function GetRefractionIn(M)
		class(Material), intent(in) :: M
		GetRefractionIn = M%refractionIn
	end function GetRefractionIn

	subroutine SetRefractionIn(M, value)
		class(Material), intent(inout) :: M
		double precision, intent(in) :: value
		M%refractionIn = value
	end subroutine SetRefractionIn

	double precision function GetRefractionOut(M)
		class(Material), intent(in) :: M
		GetRefractionOut = M%refractionOut
	end function GetRefractionOut

	subroutine SetRefractionOut(M, value)
		class(Material), intent(inout) :: M
		double precision, intent(in) :: value
		M%refractionOut = value
	end subroutine SetRefractionOut

	logical function HasTexture(M)
		class(Material), intent(in) :: M
		HasTexture = .FALSE.
	end function HasTexture

	type(RGBColor) function GetColor_MATERIAL(M)
		class(Material), intent(in) :: M
		GetColor_MATERIAL = M%color
	end function GetColor_MATERIAL

	subroutine SetColor_MATERIAL(M, color)
		class(Material), intent(inout) :: M
		type(RGBColor), intent(in) :: color
		M%color = color
	end subroutine SetColor_MATERIAL

	end module MATERIAL_
