	module PATH_

	use DEF_
	use VECTOR_

	implicit none

	ENUM, BIND(C)
		enumerator :: ROTATE_X = 1, ROTATE_Y = 2, ROTATE_Z = 3
	ENDENUM

	!
	! @section DESCRIPTION
	!
	! The PathData Class.
	! Provides the PathData class to store an ellipse as the path for animation
	! This class contains the actual data i.e used by the rendering class
	!
	!
	type, public :: Data
		double precision :: X, Y, Z
	end type Data

	type, public :: PathData
		type(Data), allocatable :: data_t(:)
		integer :: length = 0

		contains
			procedure :: init=>init_pd
			procedure :: final=>final_pd
! NOT SUPPORTED BY GFORTRAN 4.7
!
!			FINAL :: final_pd
!
	end type PathData

	!
	! @section DESCRIPTION
	!
	! The Path Class.
	! Provides the Path class to store an ellipse as the path for animation
	!
	!
	type, public :: Path
		private
			type(PathData) :: pathD
			double precision :: majorAxis = 0.0
			double precision :: minorAxis = 0.0
			double precision :: angle = 0.0
			double precision :: step_d = 0.0
			double precision :: STEP = 0.0
			integer :: length = 0
			integer :: rotationAxis = 0
			double precision :: THETA = 0.0
			double precision :: ROTATIONS = 0.0

		contains
			procedure :: SetStep
			procedure :: SetMajorAxis
			procedure :: SetMinorAxis
			procedure :: SetRotationAxis
			procedure :: SetRotations
			procedure :: SetAngle
			procedure :: SetLength

			procedure :: GetStep
			procedure :: GetMajorAxis
			procedure :: GetMinorAxis
			procedure :: GetRotationAxis
			procedure :: GetRotations
			procedure :: GetAngle
			procedure :: GetLength

			procedure :: Build=>BuildPath
			procedure :: Print=>PrintPath

			procedure :: final=>final_pa
	end type Path

	contains

	subroutine init_pd(PD, len)
		class(PathData), intent(inout) :: PD
		integer, intent(in) :: len
		allocate(PD%data_t(len))
		PD%length = len
	end subroutine init_pd

	subroutine final_pd(PD)
		class(PathData), intent(inout) :: PD
		if ( allocated(PD%data_t) ) then
			deallocate(PD%data_t)
		end if
	end subroutine final_pd

	subroutine final_pa(P)
		class(Path), intent(inout) :: P
	end subroutine final_pa

	subroutine SetStep(P, s)
		class(Path), intent(inout) :: P
		double precision, intent(in) :: s
		P%step_d = s
		return
	end subroutine SetStep

	subroutine SetMajorAxis(P, a)
		class(Path), intent(inout) :: P
		double precision, intent(in) :: a
		P%majorAxis = a
	end subroutine SetMajorAxis

	subroutine SetMinorAxis(P, b)
		class(Path), intent(inout) :: P
		double precision, intent(in) :: b
		P%minorAxis = b
	end subroutine SetMinorAxis

	subroutine SetRotationAxis(P, ra)
		class(Path), intent(inout) :: P
		integer, intent(in) :: ra
		P%rotationAxis = ra
	end subroutine SetRotationAxis

	subroutine SetRotations(P, r)
		class(Path), intent(inout) :: P
		double precision, intent(in) :: r
		P%ROTATIONS = r
	end subroutine SetRotations

	subroutine SetAngle(P, theta)
		class(Path), intent(inout) :: P
		double precision, intent(in) :: theta
		P%angle = theta
	end subroutine SetAngle

	integer function GetStep(P)
		class(Path), intent(in) :: P
		GetStep = P%step_d
	end function GetStep

	double precision function GetMajorAxis(P)
		class(Path), intent(in) :: P
		GetMajorAxis = P%majorAxis
	end function GetMajorAxis

	double precision function GetMinorAxis(P)
		class(Path), intent(in) :: P
		GetMinorAxis = P%minorAxis
	end function GetMinorAxis

	integer function GetRotationAxis(P)
		class(Path), intent(in) :: P
		GetRotationAxis = P%rotationAxis
	end function GetRotationAxis

	double precision function GetRotations(P)
		class(Path), intent(in) :: P
		GetRotations = P%ROTATIONS
	end function GetRotations

	double precision function GetAngle(P)
		class(Path), intent(in) :: P
		GetAngle = P%angle
	end function GetAngle

	integer function SetLength(P)
		class(Path), intent(inout) :: P
		double precision :: a
		double precision :: b
		double precision :: DIVISIONS

		a = P%majorAxis
		b = P%minorAxis
		!
		! Circumference of an ellipse:
		! PI * sqrt( 2 * (a*a + b*b) )
		! where 'a' and 'b' are major and minor axes radii
		!
		! We compute the circumference first and then
		! the divisions/points on the ellipse circumference
		! that are used to calculate the X, Y and Z points in 3D
		!
		DIVISIONS = PI * sqrt( 2 * (a*a + b*b) )             ! Circumference
		P%STEP = ( (2 * PI) / DIVISIONS ) * P%step_d
		P%length = (DIVISIONS / P%step_d) + 1
		if (P%length .LE. 0) then
			write (*, '(A I7)') "ERROR: Path::SetLength: Setting the length, Length: ",P%length
			SetLength = -1
		else
#ifdef __DEBUG__
			write (*, '(A F9.7)') "-  step_d: ",P%step_d
			write (*,'(A F9.2 A F9.2 A F9.2 A F9.2 A I9)') "-- STEP: ",P%STEP," a: ",a," b: ",b," angle: ",P%angle," length: ",P%length
			write (*,'(A F9.2 A I2 A I2 A I2)') " DIVISIONS: ",DIVISIONS," rotationAxis: ",P%rotationAxis," R_X: ",ROTATE_X," R_Y: ",ROTATE_Y
#endif
			SetLength = 0
		end if
	end function SetLength

	integer function GetLength(P)
		class(Path), intent(in) :: P
		GetLength = P%length
	end function GetLength

	! ------------------------------------------------------------------------------------------
	!
	! An ellipse in general position can be expressed parametrically
	! as the path of a point (X(t), Y(t)), where
	!
	! X(t) = Xc + a * cos(t) * cos(theta) - b * sin(t) * sin(theta)
	! Y(t) = Yc + a * cos(t) * sin(theta) + b * sin(t) * cos(theta)
	! Z(t) = Zc + c * cos(t) * sin(theta) + c * sin(t) * cos(theta)
	!
	! as the parameter 't' varies from 0 to 2(PI) --> 0 - 360 degrees.
	! Here (Xc, Yc) is the center of the ellipse, and 'theta' is the angle
	! between the X-axis and the major axis of the ellipse (3D). 'a' and 'b'
	! are the major and minor semi axes.
	! An ellipse becomes a 3D circle (conic section) when a = b.
	!
	! Using an eclipse to get points for circle in 3D
	!
	! ------------------------------------------------------------------------------------------
	type(PathData) function BuildPath(P, len, startAngle, Xc, Yc, Zc)
		class(Path), intent(inout) :: P
		integer, intent(in) :: len
		double precision, intent(inout) :: startAngle, Xc, Yc, Zc
		double precision :: a, b, c, t
		double precision :: X, Y, Z
		integer :: i, lengthL

		a = P%majorAxis
		b = P%minorAxis
		t = P%angle
		t = t * (PI / 180.0)
		startAngle = startAngle * (PI / 180.0)
		lengthL = len

		!
		! If length of the path is less than the longest path then
		! stretch the length to the longest path for smooth animation
		!
#ifdef __DEBUG__
		write (*, '(A F7.2 A F7.2 A F7.2)') "Xc: ",Xc," Yc: ",Yc," Zc: ",Zc
		write (*, '(A I7 A I7 A F7.2 A F7.2)') "len: ",len," length: ",P%length," STEP: ",P%STEP," ROTATIONS: ",P%ROTATIONS
#endif
		if (len .GT. P%length) then
			lengthL = len
		end if
#ifdef __DEBUG__
		write (*, '(A I7 A I7 A F7.2 A F7.2)') "len: ",len," length: ",P%length," STEP: ",P%STEP," ROTATIONS: ",P%ROTATIONS
#endif

		!
		! Build the path
		!
		lengthL = lengthL * P%ROTATIONS
		call P%PathD%init(lengthL)
		P%THETA = startAngle
		! Compute theta with respect to the position
		if (P%rotationAxis .EQ. ROTATE_X) then
			Xc = Xc - ( a * cos(P%THETA) )
			Yc = Yc - ( b * cos(t) * sin(P%THETA) )
			Zc = Zc - ( b * sin(t) * sin(P%THETA) )
			do i = 1, P%pathD%length
				X = Xc + ( a * cos(P%THETA) )
				Y = Yc + ( b * cos(t) * sin(P%THETA) )
				Z = Zc + ( b * sin(t) * sin(P%THETA) )
				P%pathD%data_t(i)%X = X
				P%pathD%data_t(i)%Y = Y
				P%pathD%data_t(i)%Z = Z
				if (P%THETA .GE. 2*PI) then
					P%THETA = 0.0
				end if
				P%THETA = P%THETA + P%STEP
			end do
		else if (P%rotationAxis .EQ. ROTATE_Y) then
			Xc = Xc - ( a * cos(t) * cos(P%THETA) )
			Yc = Yc - ( b * sin(P%THETA) )
			Zc = Zc - ( a * sin(t) * cos(P%THETA) )
			do i = 1, P%pathD%length
				X = Xc + ( a * cos(t) * cos(P%THETA) )
				Y = Yc + ( b * sin(P%THETA) )
				Z = Zc + ( a * sin(t) * cos(P%THETA) )
				P%pathD%data_t(i)%X = X
				P%pathD%data_t(i)%Y = Y
				P%pathD%data_t(i)%Z = Z
				if (P%THETA .GE. 2*PI) then
					P%THETA = 0.0
				end if
				P%THETA = P%THETA + P%STEP
			end do
		else if (P%rotationAxis .EQ. ROTATE_Z) then
			c = (a + b) / 2.0
			Xc = Xc - ( a * cos(t) * cos(P%THETA) )
			Yc = Yc - ( b * cos(t) * sin(P%THETA) )
			Zc = Zc - ( c * cos(P%THETA) * sin(P%THETA) )
			do i = 1, P%pathD%length
				X = Xc + ( a * cos(t) * cos(P%THETA) )
				Y = Yc + ( b * cos(t) * sin(P%THETA) )
				Z = Zc + ( c * cos(P%THETA) * sin(P%THETA) )
				P%pathD%data_t(i)%X = X
				P%pathD%data_t(i)%Y = Y
				P%pathD%data_t(i)%Z = Z
				if (P%THETA .GE. 2*PI) then
					P%THETA = 0.0
				end if
				P%THETA = P%THETA + P%STEP
			end do
		end if

		BuildPath = P%pathD
	end function BuildPath

	subroutine PrintPath(P)
		class(Path), intent(in) :: P
		integer :: i
		character(len=*), parameter :: FMT = '(A I9 A F14.7 A F14.7 A F14.7)'

		write (*, '(A I7)') "Length of path: ",P%pathD%length
		do i = 1, P%pathD%length
			write (*, FMT) "i: ",i,": X: ",P%pathD%data_t(i)%X," Y: ",P%pathD%data_t(i)%Y," Z: ",P%pathD%data_t(i)%Z
		end do
	end subroutine PrintPath

	end module PATH_
