	module RGBCOLOR_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The RGBColor Class.
	! Provides the RGBColor class ....
	!
	!
	type, public :: RGBColor
		double precision :: red = 0.0
		double precision :: green = 0.0
		double precision :: blue = 0.0

		contains
			procedure :: SetColor=>SetColor_RGBCOLOR
			procedure :: power
			procedure :: init=>init_rgb
	end type RGBColor

	interface operator(+)
		module procedure addReal_RGBCOLOR, addDouble_RGBCOLOR, addColor_RGBCOLOR
	end interface
	interface operator(-)
		module procedure subtractReal_RGBCOLOR, subtractDouble_RGBCOLOR, subtractColor_RGBCOLOR
	end interface
	interface operator(*)
		module procedure multiplyReal_RGBCOLOR, multiplyDouble_RGBCOLOR, multiplyColor_RGBCOLOR
	end interface
	interface operator(/)
		module procedure divideReal_RGBCOLOR, divideDouble_RGBCOLOR
	end interface
	interface operator(==)
		module procedure isEqual_RGBCOLOR
	end interface

	contains

	subroutine init_rgb(C)
		class(RGBColor), intent(inout) :: C
		C%red   = 0.0
		C%green = 0.0
		C%blue  = 0.0
	end subroutine init_rgb

	function addReal_RGBCOLOR(C, R)
		class(RGBColor), intent(in) :: C
		real, intent(in) :: R
		type(RGBColor) :: addReal_RGBCOLOR
		addReal_RGBCOLOR%red   = C%red + R
		addReal_RGBCOLOR%green = C%green + R
		addReal_RGBCOLOR%blue  = C%blue + R
	end function addReal_RGBCOLOR

	function addDouble_RGBCOLOR(C, R)
		class(RGBColor), intent(in) :: C
		double precision, intent(in) :: R
		type(RGBColor) :: addDouble_RGBCOLOR
		addDouble_RGBCOLOR%red   = C%red + R
		addDouble_RGBCOLOR%green = C%green + R
		addDouble_RGBCOLOR%blue  = C%blue + R
	end function addDouble_RGBCOLOR

	function addColor_RGBCOLOR(C1, C2)
		class(RGBColor), intent(in) :: C1, C2
		type(RGBColor) :: addColor_RGBCOLOR
		addColor_RGBCOLOR%red   = C1%red + C2%red
		addColor_RGBCOLOR%green = C1%green + C2%green
		addColor_RGBCOLOR%blue  = C1%blue + C2%blue
	end function addColor_RGBCOLOR

	function subtractReal_RGBCOLOR(C, R)
		class(RGBColor), intent(in) :: C
		real, intent(in) :: R
		type(RGBColor) :: subtractReal_RGBCOLOR
		subtractReal_RGBCOLOR%red   = C%red - R
		subtractReal_RGBCOLOR%green = C%green - R
		subtractReal_RGBCOLOR%blue  = C%blue - R
	end function subtractReal_RGBCOLOR

	function subtractDouble_RGBCOLOR(C, R)
		class(RGBColor), intent(in) :: C
		double precision, intent(in) :: R
		type(RGBColor) :: subtractDouble_RGBCOLOR
		subtractDouble_RGBCOLOR%red   = C%red - R
		subtractDouble_RGBCOLOR%green = C%green - R
		subtractDouble_RGBCOLOR%blue  = C%blue - R
	end function subtractDouble_RGBCOLOR

	function subtractColor_RGBCOLOR(C1, C2)
		class(RGBColor), intent(in) :: C1, C2
		type(RGBColor) :: subtractColor_RGBCOLOR
		subtractColor_RGBCOLOR%red   = C1%red - C2%red
		subtractColor_RGBCOLOR%green = C1%green - C2%green
		subtractColor_RGBCOLOR%blue  = C1%blue - C2%blue
	end function subtractColor_RGBCOLOR

	function multiplyReal_RGBCOLOR(C, R)
		class(RGBColor), intent(in) :: C
		real, intent(in) :: R
		type(RGBColor) :: multiplyReal_RGBCOLOR
		multiplyReal_RGBCOLOR%red   = C%red * R
		multiplyReal_RGBCOLOR%green = C%green * R
		multiplyReal_RGBCOLOR%blue  = C%blue * R
	end function multiplyReal_RGBCOLOR

	function multiplyDouble_RGBCOLOR(C, R)
		class(RGBColor), intent(in) :: C
		double precision, intent(in) :: R
		type(RGBColor) :: multiplyDouble_RGBCOLOR
		multiplyDouble_RGBCOLOR%red   = C%red * R
		multiplyDouble_RGBCOLOR%green = C%green * R
		multiplyDouble_RGBCOLOR%blue  = C%blue * R
	end function multiplyDouble_RGBCOLOR

	function multiplyColor_RGBCOLOR(C1, C2)
		class(RGBColor), intent(in) :: C1, C2
		type(RGBColor) :: multiplyColor_RGBCOLOR
		multiplyColor_RGBCOLOR%red   = C1%red * C2%red
		multiplyColor_RGBCOLOR%green = C1%green * C2%green
		multiplyColor_RGBCOLOR%blue  = C1%blue * C2%blue
	end function multiplyColor_RGBCOLOR

	function divideReal_RGBCOLOR(C, R)
		class(RGBColor), intent(in) :: C
		real, intent(in) :: R
		type(RGBColor) :: divideReal_RGBCOLOR
		divideReal_RGBCOLOR%red   = C%red / R
		divideReal_RGBCOLOR%green = C%green / R
		divideReal_RGBCOLOR%blue  = C%blue / R
	end function divideReal_RGBCOLOR

	function divideDouble_RGBCOLOR(C, R)
		class(RGBColor), intent(in) :: C
		double precision, intent(in) :: R
		type(RGBColor) :: divideDouble_RGBCOLOR
		divideDouble_RGBCOLOR%red   = C%red / R
		divideDouble_RGBCOLOR%green = C%green / R
		divideDouble_RGBCOLOR%blue  = C%blue / R
	end function divideDouble_RGBCOLOR

	function isEqual_RGBCOLOR(C1, C2)
		class(RGBColor), intent(in) :: C1, C2
		logical :: isEqual_RGBCOLOR
		isEqual_RGBCOLOR = .FALSE.
		if ((C1%red .EQ. C2%red) .AND. (C1%green .EQ. C2%green) .AND. (C1%blue .EQ. C2%blue)) then
			isEqual_RGBCOLOR = .TRUE.
		end if
	end function isEqual_RGBCOLOR

	subroutine SetColor_RGBCOLOR(C, r, g, b)
		class(RGBColor), intent(inout) :: C
		double precision, intent(in) :: r, g, b

		C%red = r
		C%green = g
		C%blue = b

		return
	end subroutine SetColor_RGBCOLOR

	type(RGBColor) function power(C, p)
		class(RGBColor), intent(in) :: C
		double precision, intent(in) :: p
		power = RGBColor( (C%red ** p), (C%green ** p), (C%blue ** p) )
	end function power


	end module RGBCOLOR_
