	module TGA_

	use IMAGE_

	implicit none

	type, public :: Color
		integer :: red, green, blue
	end type Color

	!
	! @section DESCRIPTION
	!
	! The TGA Class.
	! Provides the TGA class ....
	!
	!
	type, public, extends(Image) :: TGA
		private
			type(Color), allocatable :: color_t(:)
			integer :: BITMAP, TYPE, ROWS, COLS, COUNT

		contains
			procedure :: init=>init_tga
			procedure :: final=>final_tga
			procedure, pass(I) :: Read=>Read_TGA
			procedure, pass(I) :: Write=>Write_TGA
			procedure, pass(I) :: SetColor=>SetColor_TGA
	end type TGA

	contains

		subroutine init_tga(I, bitmap, type, rows, cols)
			class(TGA), intent(inout) :: I
			integer, intent(in) :: bitmap, type, rows, cols
			I%BITMAP = bitmap
			I%TYPE = type
			I%ROWS = rows
			I%COLS = cols
			I%COUNT = 1
			allocate ( I%color_t(rows * cols) )
		end subroutine init_tga

		subroutine final_tga(I)
			class(TGA), intent(inout) :: I
			if (allocated(I%color_t)) then
				deallocate (I%color_t)
			end if
		end subroutine final_tga

		subroutine Read_TGA(I, filename)
			class(TGA), intent(inout) :: I
			character(len=*), intent(in) :: filename
			print *,"TGA::Read: Not yet implemneted"
		end subroutine Read_TGA

		subroutine Write_TGA(I, filename)
			class(TGA), intent(inout) :: I
			character(len=*), intent(in) :: filename
			integer, parameter :: hex_1 = Z'00FF'
			integer, parameter :: hex_2 = Z'FF00'
			character :: null, char1, char2, char3
			integer :: c

			open(unit=12, file=filename, status='replace', form='formatted', action='write', err=1001)

			null = achar(0)
			write(12, '(A A)', advance='no') null,null                            ! Space for TGA header
			char1 = achar(I%TYPE)
			write(12, '(A)', advance='no') char1                                  ! Type of file format
			write(12, '(A A A A A)', advance='no') null,null,null,null,null       ! Color map specification = 0
			write(12, '(A A)', advance='no') null,null                            ! Origin X
			write(12, '(A A)', advance='no') null,null                            ! Origin Y
			char1 = achar(IAND(I%ROWS, hex_1))
			char2 = achar(IAND(I%ROWS, hex_2) / 256)
			write(12, '(A A)', advance='no') char1, char2                         ! Number of rows / width of image
			char1 = achar(IAND(I%COLS, hex_1))
			char2 = achar(IAND(I%COLS, hex_2) / 256)
			write(12, '(A A)', advance='no') char1, char2                         ! Number of cols / height of image
			char1 = achar(I%BITMAP)
			write(12, '(A)', advance='no') char1                                  ! Bitmap / Depth
			write(12, '(A)', advance='no') null                                   ! Image decriptor byte - origin in lower left-hand corner
			do c = 1, (I%ROWS * I%COLS)                                           ! Image data field - colors in RGB
				char1 = achar(I%color_t(c)%blue)
				char2 = achar(I%color_t(c)%green)
				char3 = achar(I%color_t(c)%red)
				write(12, '(A A A)', advance='no') char1, char2, char3
			end do
			write(12, '()')
			goto 1003

1001		print *,"ERROR: TGA::Write: Writing/Opening file ",filename
1003		close(unit=12)
		end subroutine Write_TGA

		subroutine SetColor_TGA(I, r, g, b)
			class(TGA), intent(inout) :: I
			integer, intent(in) :: r, g, b
			if ( I%COUNT .LE. (I%ROWS * I%COLS) ) then
				I%color_t(I%COUNT)%red = r
				I%color_t(I%COUNT)%green = g
				I%color_t(I%COUNT)%blue = b
				I%COUNT = I%COUNT + 1
			else
				write (*, '(A I7 A I7)') "Error writing colors: COUNT: ",I%COUNT," 3*ROWS*COLS: ",(I%ROWS * I%COLS)
			end if
		end subroutine SetColor_TGA

	end module TGA_
