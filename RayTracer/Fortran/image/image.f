	module IMAGE_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The Image Class.
	! Provides the Image class ....
	!
	!
	type, public, abstract :: Image
		contains
			procedure(Read_d), pass(I), deferred :: Read
			procedure(Write_d), pass(I), deferred :: Write
			procedure(SetColor_d), pass(I), deferred :: SetColor
	end type Image

	abstract interface
		subroutine Read_d(I, filename)
			import Image
			class(Image), intent(inout) :: I
			character(len=*), intent(in) :: filename
		end subroutine Read_d
		subroutine Write_d(I, filename)
			import Image
			class(Image), intent(inout) :: I
			character(len=*), intent(in) :: filename
		end subroutine Write_d
		subroutine SetColor_d(I, r, g, b)
			import Image
			class(Image), intent(inout) :: I
			integer, intent(in) :: r, g, b
		end subroutine SetColor_d
	end interface

	end module IMAGE_
