	module VECTOR_

	implicit none

	!
	! @section DESCRIPTION
	!
	! A 3D Vector class for basic trignometry
	!
	!
	type, public :: Vector
		double precision :: x, y, z;

		contains
			generic :: init => init_with_real_VECTOR, init_with_vector_VECTOR
			procedure :: init_with_real_VECTOR
			procedure :: init_with_vector_VECTOR
			procedure :: Cross
			procedure :: Length
			procedure :: Normalize
	end type Vector

	interface operator(+)
		module procedure addReal_VECTOR, addDouble_VECTOR, addVector_VECTOR
	end interface
	interface operator(-)
		module procedure subtractReal_VECTOR, subtractDouble_VECTOR, subtractVector_VECTOR
	end interface
	interface operator(*)
		module procedure multiplyReal_VECTOR, multiplyDouble_VECTOR, multiplyVector_VECTOR
	end interface
	interface operator(/)
		module procedure divideReal_VECTOR, divideDouble_VECTOR
	end interface

	interface assignment(=)
		module procedure assign_VECTOR
	end interface

	interface init_vector
		module procedure init_with_real_VECTOR
		module procedure init_with_vector_VECTOR
	end interface

	contains

	function addReal_VECTOR(V, R)
		class(Vector), intent(in) :: V
		real, intent(in) :: R
		type(Vector) :: addReal_VECTOR
		addReal_VECTOR%x = V%x + R
		addReal_VECTOR%y = V%y + R
		addReal_VECTOR%z = V%z + R
	end function addReal_VECTOR

	function addDouble_VECTOR(V, R)
		class(Vector), intent(in) :: V
		double precision, intent(in) :: R
		type(Vector) :: addDouble_VECTOR
		addDouble_VECTOR%x = V%x + R
		addDouble_VECTOR%y = V%y + R
		addDouble_VECTOR%z = V%z + R
	end function addDouble_VECTOR

	function addVector_VECTOR(V1, V2)
		class(Vector), intent(in) :: V1, V2
		type(Vector) :: addVector_VECTOR
		addVector_VECTOR%x = V1%x + V2%x
		addVector_VECTOR%y = V1%y + V2%y
		addVector_VECTOR%z = V1%z + V2%z
	end function addVector_VECTOR

	function subtractReal_VECTOR(V, R)
		class(Vector), intent(in) :: V
		real, intent(in) :: R
		type(Vector) :: subtractReal_VECTOR
		subtractReal_VECTOR%x = V%x - R
		subtractReal_VECTOR%y = V%y - R
		subtractReal_VECTOR%z = V%z - R
	end function subtractReal_VECTOR

	function subtractDouble_VECTOR(V, R)
		class(Vector), intent(in) :: V
		double precision, intent(in) :: R
		type(Vector) :: subtractDouble_VECTOR
		subtractDouble_VECTOR%x = V%x - R
		subtractDouble_VECTOR%y = V%y - R
		subtractDouble_VECTOR%z = V%z - R
	end function subtractDouble_VECTOR

	function subtractVector_VECTOR(V1, V2)
		class(Vector), intent(in) :: V1, V2
		type(Vector) :: subtractVector_VECTOR
		subtractVector_VECTOR%x = V1%x - V2%x
		subtractVector_VECTOR%y = V1%y - V2%y
		subtractVector_VECTOR%z = V1%z - V2%z
	end function subtractVector_VECTOR

	function multiplyReal_VECTOR(V, R)
		class(Vector), intent(in) :: V
		real, intent(in) :: R
		type(Vector) :: multiplyReal_VECTOR
		multiplyReal_VECTOR%x = V%x * R
		multiplyReal_VECTOR%y = V%y * R
		multiplyReal_VECTOR%z = V%z * R
	end function multiplyReal_VECTOR

	function multiplyDouble_VECTOR(V, R)
		class(Vector), intent(in) :: V
		double precision, intent(in) :: R
		type(Vector) :: multiplyDouble_VECTOR
		multiplyDouble_VECTOR%x = V%x * R
		multiplyDouble_VECTOR%y = V%y * R
		multiplyDouble_VECTOR%z = V%z * R
	end function multiplyDouble_VECTOR

	double precision function multiplyVector_VECTOR(V1, V2)
		class(Vector), intent(in) :: V1, V2
		multiplyVector_VECTOR   = (V1%x * V2%x) + (V1%y * V2%y) + (V1%z * V2%z)
	end function multiplyVector_VECTOR

	function divideReal_VECTOR(V, R)
		class(Vector), intent(in) :: V
		real, intent(in) :: R
		type(Vector) :: divideReal_VECTOR
		divideReal_VECTOR%x = V%x / R
		divideReal_VECTOR%y = V%y / R
		divideReal_VECTOR%z = V%z / R
	end function divideReal_VECTOR

	function divideDouble_VECTOR(V, R)
		class(Vector), intent(in) :: V
		double precision, intent(in) :: R
		type(Vector) :: divideDouble_VECTOR
		divideDouble_VECTOR%x = V%x / R
		divideDouble_VECTOR%y = V%y / R
		divideDouble_VECTOR%z = V%z / R
	end function divideDouble_VECTOR

	subroutine assign_VECTOR(V1, V2)
		class(Vector), intent(inout) :: V1
		class(Vector), intent(in) :: V2
		V1%x = V2%x
		V1%y = V2%y
		V1%z = V2%z
	end subroutine assign_VECTOR

	type(Vector) function Cross(V1, V2)
		class(Vector), intent(in) :: V1, V2
		Cross%x = (V1%y * V2%z) - (V1%z * V2%y)
		Cross%y = (V1%z * V2%x) - (V1%x * V2%z)
		Cross%z = (V1%x * V2%y) - (V1%y * V2%x)
		return
	end function Cross

	double precision function Length(V)
		class(Vector), intent(in) :: V
		Length = sqrt( (V%x * V%x) + (V%y * V%y) + (V%z * V%z) )
		return
	end function Length

	subroutine Normalize(V)
		class(Vector), intent(inout) :: V
		double precision :: length
		length = sqrt((V%x * V%x) + (V%y * V%y) + (V%z * V%z))
		V%x = V%x / length;
		V%y = V%y / length;
		V%z = V%z / length;
		return
	end subroutine Normalize

	subroutine init_with_real_VECTOR(V, X, Y, Z)
		class(Vector), intent(inout) :: V
		double precision, intent(in) :: X, Y, Z

		V%x = X
		V%y = Y
		V%z = Z
		return
	end subroutine init_with_real_VECTOR

	subroutine init_with_vector_VECTOR(V1, V2)
		class(Vector), intent(inout) :: V1
		class(Vector), intent(in) :: V2

		V1%x = V2%x
		V1%y = V2%y
		V1%z = V2%z
		return
	end subroutine init_with_vector_VECTOR

	end module VECTOR_
