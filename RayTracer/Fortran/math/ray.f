	module RAY_

	use VECTOR_
	use MATERIAL_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The HitInfo Class.
	! Provides the HitInfo class ....
	!
	!
	type, public :: HitInfo
		type(Vector) :: Position = Vector(0.0,0.0,0.0)
		type(Vector) :: Normal = Vector(0.0,0.0,0.0)
		type(Material) :: Material_t
		double precision :: Distance = 0.0
	end type HitInfo

	!
	! @section DESCRIPTION
	!
	! The Ray Class.
	! Provides the Ray class ....
	!
	!
	type, public :: Ray
		type(Vector) :: origin
		type(Vector) :: dir
		type(HitInfo) :: hitInfo_t

		contains
			generic :: init => init_with_vector_RAY, init_with_ray_RAY
			procedure :: init_with_vector_RAY
			procedure :: init_with_ray_RAY
	end type Ray

	interface assignment(=)
		module procedure assign_RAY
	end interface

	interface init
		module procedure init_with_vector_RAY
		module procedure init_with_ray_RAY
	end interface

	contains

	subroutine assign_RAY(R1, R2)
		class(Ray), intent(inout) :: R1
		class(Ray), intent(in) :: R2
		R1%origin = R2%origin
		R1%dir = R2%dir
		return
	end subroutine assign_RAY

	subroutine init_with_vector_RAY(R, o, d)
		class(Ray), intent(inout) :: R
		class(Vector), intent(in) :: o, d

		R%origin = o
		R%dir = d
		return
	end subroutine init_with_vector_RAY

	subroutine init_with_ray_RAY(R, R2)
		class(Ray), intent(inout) :: R
		class(Ray), intent(in) :: R2

		R%origin = R2%origin
		R%dir = R2%dir
		return
	end subroutine init_with_ray_RAY

	end module RAY_
