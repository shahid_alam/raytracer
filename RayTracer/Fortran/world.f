	module WORLD_

	use DEF_
	use COMMON_
	use VECTOR_
	use RGBCOLOR_
	use SPHERE_
	use PLANE_
	use LIGHT_
	use MATERIAL_
	use PATH_
	use SCENE_
	use TGA_
	use RAYTRACER_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The World Class.
	! Provides the World class ....
	!
	!
	type, public :: World
		private
			type(Scene) :: S

		contains
			procedure :: Build=>BuildScene
			procedure :: init=>init_wo
			procedure :: final=>final_wo
			procedure :: RenderScene
			procedure :: RenderAnimation
	end type World

	contains

	subroutine init_wo(W)
		class(World), intent(inout) :: W
		call W%S%init()
	end subroutine init_wo

	subroutine final_wo(W)
		class(World), intent(inout) :: W
		call W%S%final()
	end subroutine final_wo

	subroutine BuildScene(W, filename)
		class(World), intent(inout) :: W
		character(len=*), intent(in) :: filename
		type(Sphere)  :: sphere_t
		type(Plane) :: plane_t
		type(Light) :: light_t
		type(Vector) :: v
		type(Material) :: m
		type(RGBColor) :: c
		type(Path) :: p
		integer :: i, numSpheres, numPlanes, numLights

		if (W%S%Read(filename) .LE. 0) then
			print *,"World::Build: Error Reading File ",filename
#ifdef __DEBUG__
		else
			call W%S%Print()

			numSpheres = W%S%GetNumberOfSpheres()
			numPlanes = W%S%GetNumberOfPlanes()
			numLights = W%S%GetNumberOfLights()

			!
			! Printing all the spheres in the scene
			!
			print *,"Spheres:"
			do i = 1, numSpheres
				sphere_t = W%S%GetSphere(i)
				write (*,'(A I3 A)') " Sphere[",i,"]:"
				v = sphere_t%GetCenter()
				write (*,'(A F9.2 A F9.2 A F9.2)') "            Center: ",v%x," ",v%y," ",v%z
				write               (*,'(A F9.2)') "            Radius: ",sphere_t%GetRadius()
				write               (*,'(A F9.2)') "        StartAngle: ",sphere_t%GetStartAngle()
				m = sphere_t%GetMaterial()
				write (*,'(A)') "   Material:"
				write (*,'(A F9.2)') "        Reflection: ",m%GetReflection()
				write (*,'(A F9.2)') "      RefractionIn: ",m%GetRefractionIn()
				write (*,'(A F9.2)') "     RefractionOut: ",m%GetRefractionOut()
				write (*,'(A F9.2)') "      Transparency: ",m%GetTransparency()
				c = m%GetColor()
				write (*,'(A F9.2 A F9.2 A F9.2)') "             Color: ",c%red," ",c%green," ",c%blue
				if (sphere_t%HasPath()) then
					p = sphere_t%GetPath()
					write (*,'(A)') "   Path:"
					write   (*,'(A I9)') "              Step: ",p%GetStep()
					write (*,'(A F9.2)') "         MajorAxis: ",p%GetMajorAxis()
					write (*,'(A F9.2)') "         MinorAxis: ",p%GetMinorAxis()
					write   (*,'(A I9)') "      RotationAxis: ",p%GetRotationAxis()
					write (*,'(A F9.2)') "             Angle: ",p%GetAngle()
					write   (*,'(A I9)') "            Length: ",p%GetLength()
				end if
			end do
			!
			! Printing all the planes in the scene
			!
			print *,"Planes:"
			do i = 1, numPlanes
				plane_t = W%S%GetPlane(i)
				write (*,'(A I3 A)') " Plane[",i,"]:"
				v = plane_t%GetPoint()
				write (*,'(A F9.2 A F9.2 A F9.2)') "             Point: ",v%x," ",v%y," ",v%z
				v = plane_t%GetNormalVector()
				write (*,'(A F9.2 A F9.2 A F9.2)') "     Normal Vector: ",v%x," ",v%y," ",v%z
				m = plane_t%GetMaterial()
				write (*,'(A)') "   Material:"
				write (*,'(A F9.2)') "        Reflection: ",m%GetReflection()
				write (*,'(A F9.2)') "      RefractionIn: ",m%GetRefractionIn()
				write (*,'(A F9.2)') "     RefractionOut: ",m%GetRefractionOut()
				write (*,'(A F9.2)') "      Transparency: ",m%GetTransparency()
				c = m%GetColor()
				write (*,'(A F9.2 A F9.2 A F9.2)') "             Color: ",c%red," ",c%green," ",c%blue
				if (plane_t%HasPath()) then
					p = plane_t%GetPath()
					write (*,'(A)') "   Path:"
					write   (*,'(A I9)') "             Step:  ",p%GetStep()
					write (*,'(A F9.2)') "        MajorAxis:  ",p%GetMajorAxis()
					write (*,'(A F9.2)') "        MinorAxis:  ",p%GetMinorAxis()
					write   (*,'(A I9)') "      RotationAxis: ",p%GetRotationAxis()
					write (*,'(A F9.2)') "             Angle: ",p%GetAngle()
					write   (*,'(A I9)') "            Length: ",p%GetLength()
				end if
			end do

			!
			! Printing all the lights in the scene
			!
			print *,"Lights:"
			do i = 1, numLights
				light_t = W%S%GetLight(i)
				write (*,'(A I3 A)') " Light[",i,"]:"
				v = light_t%GetPosition()
				write (*,'(A F9.2 A F9.2 A F9.2)') "          Position: ",v%x," ",v%y," ",v%z
				c = light_t%GetColor()
				write (*,'(A F9.2 A F9.2 A F9.2)') "             Color: ",c%red," ",c%green," ",c%blue
			end do

#endif
		end if

	end subroutine BuildScene

	! ------------------------------------------------------------------------------------------
	!
	! Build a TGA image file
	!
	! Perspective viewing produces more realistic images than orthographic viewing
	! By default the VIEWING is 0 i.e; orhtographic viewing
	! Use VIEWING_TYPE = 1 for perspective viewing
	!
	!
	! ------------------------------------------------------------------------------------------
	logical function RenderScene(W, file)
		class(World), intent(in) :: W
		character(len=*), intent(in) :: file
		type(RayTracer) :: rayTracer_t
		type(TGA) :: tga_t
		type(Ray) :: ray_t
		type(RGBColor) :: color
		double precision :: distance_local, ZOOM, INV_GAMMA, coef, t
		integer :: x, y, VIEWING_TYPE, red, green, blue, depth

		call rayTracer_t%init(W%S)
		! 24 bit RGB uncompressed TGA image
		call tga_t%init(24, 2, W%S%GetWidth(), W%S%GetHeight())
		distance_local = DISTANCE

		VIEWING_TYPE = W%S%GetViewType()
		ZOOM = 1.0 / W%S%GetZoom()
		INV_GAMMA = 1.0 / W%S%GetGamma()
		do y = 0, W%S%GetHeight()-1
			do x = 0, W%S%GetWidth()-1
				coef = 1.0
				depth = 3
				t = 10000.0
				color = W%S%GetBGColor()
				if (VIEWING_TYPE .EQ. ORTHOGRAPHIC) then
					ray_t%origin = Vector(ZOOM * x, ZOOM * y, -distance_local)
					ray_t%dir = Vector(0.0, 0.0, distance)
				else if (VIEWING_TYPE == PERSPECTIVE) then
					ray_t%origin = Vector(W%S%GetWidth() / 2.0, W%S%GetHeight() / 2.0, -distance_local)
					ray_t%dir = Vector(ZOOM * (x - W%S%GetWidth() / 2.0 + 0.5), ZOOM * (y - W%S%GetHeight() / 2.0 + 0.5), distance_local)
				end if
				call ray_t%dir%Normalize()
				RenderScene = rayTracer_t%RayTrace(ray_t, color, t, coef, depth)

				!
				! Gamma correction
				! Color = Color ^ (1 / gamma)
				!
				color = color%power(INV_GAMMA)
				color = color * 250.0
				color%red = min(color%red, 255.0)
				color%green = min(color%green, 255.0)
				color%blue = min(color%blue, 255.0)
				!
				! Fill the TGA image buffer
				!
				red   = color%red
				green = color%green
				blue  = color%blue
				call tga_t%SetColor(red, green, blue)
#ifdef __TESTING__
				write (*,'(A F7.2 A F7.2 A F7.2)') "Color: ",color%red," ",color%green," ",color%blue
				write (*,'(A I3 A I3 A I3)') "Color: ",red," ",green," ",blue
#endif
			end do
		end do

		! Write the TGA image buffer to the file
		call tga_t%Write (file)

		call tga_t%final()
		RenderScene = .TRUE.
	end function RenderScene

	! ------------------------------------------------------------------------------------------
	!
	! Build TGA image files for animation
	!
	! Perspective viewing produces more realistic images than orthographic viewing
	! By default the VIEWING is 0 i.e; orhtographic viewing
	! Use VIEWING_TYPE = 1 for perspective viewing
	!
	!
	! ------------------------------------------------------------------------------------------
	logical function RenderAnimation(W, dir)
		class(World), intent(inout) :: W
		character(len=*), intent(in) :: dir
		type(Sphere) :: sphere_t
		type(Plane) :: plane_t
		type(Path) :: path_t
		type(Path) :: tempP
		type(PathData), pointer :: tempPD_t => NULL()
		type(Vector) :: tempV
		character(len=256) :: file, number
		integer :: i, count, LENGTH, LEN, numSpheres, numPlanes, numPaths
		double precision :: angle, x, y, z
		real :: time_start, time_end

		LENGTH = 0

		!
		! Getting the longest path
		!
		print *,"Getting the longest path"
		numPaths = W%S%GetNumberOfPaths()
		do i = 1, numPaths
			path_t = W%S%GetPath(i)
			! Find the longest path
			if (path_t%GetLength() .GT. LENGTH) then
				LENGTH = path_t%GetLength()
			end if
		end do

		numSpheres = W%S%GetNumberOfSpheres()
		numPlanes = W%S%GetNumberOfPlanes()

		!
		! Building the path(s)
		!
		LEN = LENGTH
		write (*, '(A I9)') "Building the path(s): The longest path length: ",LENGTH
		do i = 1, numSpheres
			sphere_t = W%S%GetSphere(i)
			if (sphere_t%HasPath()) then
				tempP = sphere_t%GetPath()
				tempV = sphere_t%GetPosition()
				angle = sphere_t%GetStartAngle()
				allocate(tempPD_t)
				tempPD_t = tempP%Build(LENGTH, angle, tempV%x, tempV%y, tempV%z)
				call W%S%SetPathDataSphere(i, tempPD_t)
#ifdef __TESTING__
				call tempP%print()
#endif
				! Find the longest path after adjusting during the build
				if (tempPD_t%length .GT. LEN) then
					LEN = tempPD_t%length
				end if
			end if
		end do
		do i = 1, numPlanes
			plane_t = W%S%GetPlane(i)
			if (plane_t%HasPath()) then
				tempP = plane_t%GetPath()
				tempV = plane_t%GetPosition()
				angle = plane_t%GetStartAngle()
				allocate(tempPD_t)
				tempPD_t = tempP%Build(LENGTH, angle, tempV%x, tempV%y, tempV%z)
				call W%S%SetPathDataPlane(i, tempPD_t)
#ifdef __DEBUG__
				call tempP%print()
#endif
				! Find the longest path after adjusting during the build
				if (tempPD_t%length .GT. LEN) then
					LEN = tempPD_t%length
				end if
			end if
		end do
		if (LEN .LE. 0) then
			LEN = 1
		end if
		LENGTH = LEN

		!
		! Rendering the images for the subsequent positions
		!
		write (*, '(A I9)') "Rendering the images: Longest path length after adjusting:",LENGTH
		do count = 1, LENGTH
			call cpu_time (time_start)
			!
			! TIMING
			!
			do i = 1, numSpheres
				sphere_t = W%S%sphere_t(i)
				if (sphere_t%HasPathData()) then
					call sphere_t%GetPathData(tempPD_t)
					if (count .LT. tempPD_t%length) then
						x = tempPD_t%data_t(count)%X
						y = tempPD_t%data_t(count)%Y
						z = tempPD_t%data_t(count)%Z
						call W%S%SetPositionSphere(i, x, y, z)
#ifdef __DEBUG__
						tempV = sphere_t%GetPosition()
						write (*, '(A I9 A F14.7 A F14.7 A F14.7 A)') "count: ",count," Position: (",tempV%x," ",tempV%y," ",tempV%z,")"
#endif
					end if
				end if
			end do
			do i = 1, numPlanes
				plane_t = W%S%plane_t(i)
				if (plane_t%HasPathData()) then
					call plane_t%GetPathData(tempPD_t)
					if (count .LT. tempPD_t%length) then
						x = tempPD_t%data_t(count)%X
						y = tempPD_t%data_t(count)%Y
						z = tempPD_t%data_t(count)%Z
						call W%S%SetPositionPlane(i, x, y, z)
#ifdef __DEBUG__
						tempV = sphere_t%GetPosition()
						write (*, '(A I9 A F14.7 A F14.7 A F14.7 A)') "count: ",count," Position: (",tempV%x," ",tempV%y," ",tempV%z,")"
#endif
					end if
				end if
			end do
			!
			! Concatenating the string as in C/C++:
			! sprintf(file, "%s/test_%d.tga", dir, 10000+count);
			! 
			write (number, '(I5)') 10000+count-1
			file = TRIM(dir)//'/test_'//TRIM(number)//'.tga'
			print *,"Rendering to file: ",file
			if (.NOT. W%RenderScene(file)) then
				print *,"ERROR: World::RenderAnimation: Rendering scene"
			end if
			!
			! TIMING
			!
			call cpu_time (time_end)
			print *,( (time_end - time_start) * 1000.0 )
		end do

		!
		! Deallocating path data
		!
		do i = 1, numSpheres
			sphere_t = W%S%sphere_t(i)
			if (sphere_t%HasPathData()) then
				call sphere_t%GetPathData(tempPD_t)
				call tempPD_t%final()
			end if
		end do
		do i = 1, numPlanes
			plane_t = W%S%plane_t(i)
			if (plane_t%HasPathData()) then
				call plane_t%GetPathData(tempPD_t)
				call tempPD_t%final()
			end if
		end do
		RenderAnimation = .TRUE.
		return
	end function RenderAnimation

	end module WORLD_
