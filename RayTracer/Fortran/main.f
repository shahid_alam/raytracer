	program main

	use DEF_
	use COMMON_
use SCENE_
	use WORLD_

	implicit none

	type (World) :: W
	character(LEN=256) :: scene_filename, dir_name
	logical :: RES
	real :: time_start, time_end

	if (COMMAND_ARGUMENT_COUNT() .LT. 2) then
		print *,"Error reading the scene file or images dir"
	else
		call GET_COMMAND_ARGUMENT(1, scene_filename)
		call GET_COMMAND_ARGUMENT(2, dir_name)
		print *,"Scene file: ",TRIM(scene_filename)
		print *,"Images dir: ",TRIM(dir_name)
		call W%init()
		call W%Build(TRIM(scene_filename))
		!
		! Reading the macro __NO_ANIMATION__ passed to the compiler
		!
#ifdef __NO_ANIMATION__
			RES = W%RenderScene(TRIM(dir_name)//'/test.tga')
#else
			!
			! TIMING
			!
			call cpu_time (time_start)
			RES = W%RenderAnimation(TRIM(dir_name))
			!
			! TIMING
			!
			call cpu_time (time_end)
			print *,( (time_end - time_start) * 1000.0 )
#endif
		print *,"Finished Rendering images"
	end if

	call W%final()
#ifdef __DEBUG__
	print *,"Exiting the program"
#endif

	stop
	end program main
