	module LIGHT_

	use VECTOR_
	use RGBCOLOR_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The Light Class.
	! Provides the Light class ....
	!
	!
	type, public :: Light
		private
			type(Vector) :: point
			type(RGBColor) :: color

		contains
			procedure :: SetPosition=>SetPosition_LIGHT
			procedure :: GetPosition=>GetPosition_LIGHT
			procedure :: SetColor=>SetColor_LIGHT
			procedure :: GetColor=>GetColor_LIGHT
	end type Light

	contains

	subroutine SetPosition_LIGHT(L, p)
		class(Light), intent(inout) :: L
		type(Vector), intent(in) :: p
		L%point = p
	end subroutine SetPosition_LIGHT

	type(Vector) function GetPosition_LIGHT(L)
		class(Light), intent(in) :: L
		GetPosition_LIGHT = L%point
	end function GetPosition_LIGHT

	subroutine SetColor_LIGHT(L, c)
		class(Light), intent(inout) :: L
		type(RGBColor), intent(in) :: c
		L%color = c
	end subroutine SetColor_LIGHT

	type(RGBColor) function GetColor_LIGHT(L)
		class(Light), intent(in) :: L
		GetColor_LIGHT = L%color
	end function GetColor_LIGHT

	end module LIGHT_
