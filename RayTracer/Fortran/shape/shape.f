	module SHAPE_

	use VECTOR_
	use MATERIAL_
	use RAY_
	use PATH_

	implicit none

	real, parameter :: SHAPE_EPSILON = 0.1

	!
	! @section DESCRIPTION
	!
	! The Shape Class.
	! Provides the Shape class ....
	!
	!
	type, public, abstract :: Shape
		private
			type(Material), public :: M
			type(Ray) :: R
			type(Path) :: P
			type(PathData), pointer :: PD_t => NULL()
			logical :: isPath = .FALSE.
			logical :: isPathData = .FALSE.
			double precision :: angle = 0.0

		contains
			procedure(SetPosition_d), pass, deferred :: SetPosition
			procedure(GetPosition_d), pass, deferred :: GetPosition
			procedure(Hit_d), pass, deferred :: Hit

			procedure :: SetMaterial
			procedure :: GetMaterial
			procedure :: SetStartAngle
			procedure :: GetStartAngle
			procedure :: SetPath
			procedure :: GetPath
			procedure :: SetPathData
			procedure :: GetPathData
			procedure :: HasPath
			procedure :: HasPathData
	end type Shape

	abstract interface
		subroutine SetPosition_d(S, X, Y, Z)
			import Shape
			class(Shape), intent(inout) :: S
			double precision, intent(in) :: X, Y, Z
		end subroutine SetPosition_d
		type(Vector) function GetPosition_d(S)
			import Shape, Vector
			class(Shape), intent(in) :: S
		end function GetPosition_d
		logical function Hit_d(S, R, t)
			import Shape, Ray
			class(Shape), intent(in) :: S
			class(Ray), intent(inout) :: R
			double precision, intent(inout) :: t
		end function Hit_d
	end interface

	contains

	subroutine SetMaterial(S, Ma)
		class(Shape), intent(inout) :: S
		type(Material), intent(in) :: Ma
		S%M = Ma
	end subroutine SetMaterial

	type(Material) function GetMaterial(S)
		class(Shape), intent(in) :: S
		GetMaterial = S%M
	end function GetMaterial

	subroutine SetStartAngle(S, a)
		class(Shape), intent(inout) :: S
		double precision, intent(in) :: a
		S%angle = a
	end subroutine SetStartAngle

	double precision function GetStartAngle(S)
		class(Shape), intent(in) :: S
		GetStartAngle = S%angle
	end function GetStartAngle

	subroutine SetPath(S, Pa)
		class(Shape), intent(inout) :: S
		type(Path), intent(in) :: Pa
		S%P = Pa
		S%isPath = .TRUE.
	end subroutine SetPath

	type(Path) function GetPath(S)
		class(Shape), intent(in) :: S
		GetPath = S%P
	end function GetPath

	subroutine SetPathData(S, Pad)
		class(Shape), intent(inout) :: S
		type(PathData), target, intent(in) :: Pad
		S%PD_t => Pad
		S%isPathData = .TRUE.
	end subroutine SetPathData

	subroutine GetPathData(S, ptr)
		class(Shape), intent(in) :: S
		type(PathData), pointer, intent(out) :: ptr
		ptr => S%PD_t
	end subroutine GetPathData

	logical function HasPath(S)
		class(Shape), intent(in) :: S
		HasPath = S%isPath
	end function HasPath

	logical function HasPathData(S)
		class(Shape), intent(in) :: S
		HasPathData = S%isPathData
	end function HasPathData

	end module SHAPE_
