	module PLANE_

	use SHAPE_
	use VECTOR_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The Plane Class.
	! Provides the Plane class ....
	!
	!
	type, public, extends(Shape) :: Plane
		private
			type(Vector) :: point = Vector(0.0, 0.0, 0.0)
			type(Vector) :: normal = Vector(0.0, 0.0, 0.0)

		contains
			procedure, pass :: SetPosition=>SetPosition_PLANE
			procedure, pass :: GetPosition=>GetPosition_PLANE
			procedure, pass :: Hit=>Hit_PLANE

			procedure :: SetPoint
			procedure :: GetPoint
			procedure :: SetNormalVector
			procedure :: GetNormalVector
	end type Plane

	contains

	subroutine SetPosition_PLANE(S, X, Y, Z)
		class(Plane), intent(inout) :: S
		double precision, intent(in) :: X, Y, Z
		type(Vector) :: pt
		pt = Vector(X, Y, Z)
		call SetPoint(S, pt)
	end subroutine SetPosition_PLANE

	type(Vector) function GetPosition_PLANE(S)
		class(Plane), intent(in) :: S
		GetPosition_PLANE = S%point
	end function GetPosition_PLANE

	! -------------------------------------------------------------------------------------
	! Equation of plane:
	! (p - a) . n = 0                          (1)
	! where a is a known point that lies on the plane
	! and n is the normal to the plane.
	! p is a point either on or not on the plane.
	! if p is on the plane then
	! p is on the plane only if the vector from a to p
	! is perpendicular to n.
	!
	! Ray intersection/hit equation:
	! p = o + td                               (2)
	!
	! Substitute equation 2 into 1
	! (o + td - a) . n = 0
	! Solving for t:
	! t = [ (a - o).n ] / (d.n)
	!
	! -------------------------------------------------------------------------------------
	logical function Hit_PLANE(S, R, t)

		class(Plane), intent(in) :: S
		class(Ray), intent(inout) :: R
		double precision, intent(inout) :: t
		double precision :: d
		logical :: isHit

		d = R%dir * S%normal;
		if (d .LE. 0.0) then
			isHit = .FALSE.
		else
			d = ((S%point - R%origin) * S%normal) / d
			if ((d .GT. SHAPE_EPSILON) .AND. (d .LT. t)) then
				t = d;
				R%hitInfo_t%Position = R%origin + (R%dir * t)
				R%hitInfo_t%Normal = S%normal
				R%hitInfo_t%material_t = S%GetMaterial()
				R%hitInfo_t%Distance = t
				isHit = .TRUE.
			end if
		end if

		Hit_PLANE = isHit

	end function Hit_PLANE

	subroutine SetPoint(P, pt)
		class(Plane), intent(inout) :: P
		class(Vector), intent(in) :: pt
		P%point = pt
	end subroutine SetPoint

	type(Vector) function GetPoint(P)
		class(Plane), intent(in) :: P
		GetPoint = P%point
	end function GetPoint

	subroutine SetNormalVector(P, n)
		class(Plane), intent(inout) :: P
		class(Vector), intent(in) :: n
		P%normal = n
	end subroutine SetNormalVector

	type(Vector) function GetNormalVector(P)
		class(Plane), intent(in) :: P
		GetNormalVector = P%normal
	end function GetNormalVector

	end module PLANE_
