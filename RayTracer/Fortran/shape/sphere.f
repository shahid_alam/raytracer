	module SPHERE_

	use SHAPE_
	use VECTOR_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The Sphere Class.
	! Provides the Sphere class ....
	!
	!
	type, public, extends(Shape) :: Sphere
		private
			type(Vector) :: center = Vector(0.0, 0.0, 0.0)
			double precision :: radius = 0.0

		contains
			procedure, pass :: SetPosition=>SetPosition_SPHERE
			procedure, pass :: GetPosition=>GetPosition_SPHERE
			procedure, pass :: Hit=>Hit_SPHERE

			procedure :: SetCenter
			procedure :: GetCenter
			procedure :: SetRadius
			procedure :: GetRadius
	end type Sphere

	contains

	subroutine SetPosition_SPHERE(S, X, Y, Z)
		class(Sphere), intent(inout) :: S
		double precision, intent(in) :: X, Y, Z
		type(Vector) :: c
		c = Vector(X, Y, Z)
		call SetCenter(S, c)
	end subroutine SetPosition_SPHERE

	type(Vector) function GetPosition_SPHERE(S)
		class(Sphere), intent(in) :: S
		GetPosition_SPHERE = S%center
	end function GetPosition_SPHERE

	! -------------------------------------------------------------------------------------
	! Equation of sphere:
	! (x-cx)^2 + (y-cy)^2 + (z-cz)^2 - r^2 = 0
	! where x, y and z is any 3D point inside the sphere
	! and cx, cy and cy is the center of the sphere
	!
	! Can be writeen as:
	! (p-c) . (p-c) - r^2 = 0                (1)
	!
	! Ray intersection/hit equation:
	! p = o + td                               (2)
	!
	! Substitute equation 2 into 1
	! (o+td-c) dot (o+td-c) - r^2 = 0
	!
	! Solving above equation:
	! d*dt^2 + 2d*(o-c)t +(o-c)^2 - r^2
	! at^2 + bt + c = 0   --->  Quadratic equation
	! where a = d*d, b = 2d*(o-c) and c = (o-c)^2 - r^2
	! t = [ -b +- sqrt(b^2 - 4ac) ] / (2a)
	!
	! Value of the discriminant
	! d = b - 4ac
	! decides whether there is an intersection/hit or not
	!   d < 0   no intersection
	!   d = 0   one intersection
	!   d > 0   two intersections
	! -------------------------------------------------------------------------------------
	logical function Hit_SPHERE(S, R, t)

		class(Sphere), intent(in) :: S
		class(Ray), intent(inout) :: R
		double precision, intent(inout) :: t
		type(Vector) :: dist, normal
		double precision :: a, b, c, D, t0, t1, temp
		logical :: isHit

		dist = R%origin - S%center
		a = R%dir * R%dir
		b = R%dir * dist * 2.0
		c = dist * dist - S%radius * S%radius
		D = b * b - 4.0 * a * c

		isHit = .FALSE.
		if (D .GE. 0.0) then
			t0 = ( -b - sqrt(D) ) / ( 2.0 * a )
			t1 = ( -b + sqrt(D) ) / ( 2.0 * a )
			if ((t0 .GT. SHAPE_EPSILON) .AND. (t0 .LT. t)) then
				t = t0
				isHit = .TRUE.
			end if
			if ((t1 .GT. SHAPE_EPSILON) .AND. (t1 .LT. t)) then
				t = t1
				isHit = .TRUE.
			end if

			if (isHit) then
				R%hitInfo_t%Position = R%origin + (R%dir * t)
				normal = R%hitInfo_t%Position - S%center
				temp = normal * normal
				if (temp .LE. 0.0) then
					isHit = .FALSE.
				else
					temp = 1.0 / sqrt(temp)
					R%hitInfo_t%Normal = normal * temp
					R%hitInfo_t%Material_t = S%GetMaterial()
					R%hitInfo_t%Distance = t
				end if
			end if
		end if

		Hit_SPHERE = isHit

	end function Hit_SPHERE

	subroutine SetCenter(S, c)
		class(Sphere), intent(inout) :: S
		class(Vector), intent(in) :: c
		S%center = c
	end subroutine SetCenter

	type(Vector) function GetCenter(S)
		class(Sphere), intent(in) :: S
		GetCenter = S%center
	end function GetCenter

	subroutine SetRadius(S, r)
		class(Sphere), intent(inout) :: S
		double precision, intent(in) :: r
		S%radius = r
	end subroutine SetRadius

	double precision function GetRadius(S)
		class(Sphere), intent(in) :: S
		GetRadius = S%radius
	end function GetRadius

	end module SPHERE_
