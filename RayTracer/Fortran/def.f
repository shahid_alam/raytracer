	module DEF_

	! --------------------------------------------------------
	!
	! Viewing types
	!
	! --------------------------------------------------------
	ENUM, BIND(C)
		enumerator :: VIEWING_TYPES_SUPPORTED = 02
		! The origin of the Ray is perpendicular and far away (distance) from the scene
		enumerator :: ORTHOGRAPHIC            = 00
		! Axis-aligned perspective viewing
		! The origin of the Ray is a pin hole (centre point) and far away (distance) from the scene
		enumerator :: PERSPECTIVE             = 01
	ENDENUM

	!
	! Constants
	!
	!
	! Value of PI to 50 decimal places
	! N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).
	! N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).
	!
	double precision, parameter :: PI             = 3.14159265358979323846264338327950288419716939937510
	double precision, parameter :: DISTANCE       = 1000.0
	double precision, parameter :: REFRACTION_IN  = 1.0
	double precision, parameter :: REFRACTION_OUT = 1.25
	double precision, parameter :: EPSILON        = 0.15

	end module DEF_
