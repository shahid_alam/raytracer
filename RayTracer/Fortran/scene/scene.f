	module SCENE_

	use COMMON_
	use DEF_
	use VECTOR_
	use RGBCOLOR_
	use PARSE_
	use SPHERE_
	use PLANE_
	use LIGHT_
	use MATERIAL_
	use PATH_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The Scene Class.
	! Provides the Scene class ....
	!
	!
	type, public :: Scene
		private
			type(Sphere), public, allocatable :: sphere_t(:)
			type(Plane), public, allocatable :: plane_t(:)
			type(Material), allocatable :: material_t(:)
			type(Path), allocatable :: path_t(:)
			type(Light), public, allocatable :: light_t(:)
			integer :: version = 0
			integer :: width = 0
			integer :: height = 0
			integer :: viewType = 0
			double precision :: zoom = 0.0
			double precision :: gamma = 0.0
			integer :: numberOfMaterials = 0
			integer :: numberOfPaths = 0
			integer, public :: numberOfSpheres = 0
			integer, public :: numberOfPlanes = 0
			integer, public :: numberOfLights = 0
			integer :: SHAPE_READ = 0
			integer :: PATH_READ = 0
			logical :: SCENE_READ = .FALSE.
			integer :: SPHERE_READ = 0
			integer :: PLANE_READ = 0
			integer :: MATERIAL_READ = 0
			integer :: LIGHT_READ = 0
			integer :: COUNT = 0
			integer :: ERROR_NUMBER = 0;
			character(LEN=256) :: filename
			type(RGBColor) :: bgcolor
			type(Parse) :: parse_t

!
! Unlimited polymorphic class
! Not supported in gfortran version 4.7
!
!			class(*), public, pointer :: objects(:)

		contains
			procedure :: Read
			procedure :: Print=>PrintScene

			procedure :: GetVersion
			procedure :: GetWidth
			procedure :: GetHeight
			procedure :: GetViewType
			procedure :: GetZoom
			procedure :: SetZoom
			procedure :: GetGamma
			procedure :: GetBGColor
			procedure :: GetNumberOfShapes
			procedure :: GetNumberOfSpheres
			procedure :: GetNumberOfPaths
			procedure :: GetNumberOfPlanes
			procedure :: GetNumberOfLights
			procedure :: GetSphere
			procedure :: GetPlane
			procedure :: GetMaterial=>GetMaterial_SCENE
			procedure :: GetPath=>GetPath_SCENE
			procedure :: GetLight

			procedure :: SetPathDataSphere
			procedure :: SetPathDataPlane
			procedure :: SetPositionSphere
			procedure :: SetPositionPlane

			procedure :: init=>init_sc
			procedure :: final=>final_sc

	end type Scene

	contains

	subroutine init_sc(S)
		class(Scene), intent(inout) :: S
		S%version = 0
		S%width = 0
		S%height = 0
		S%viewType = 0
		S%zoom = 0.0
		S%gamma = 0.0
		S%numberOfMaterials = 0
		S%numberOfPaths = 0
		S%numberOfSpheres = 0
		S%numberOfPlanes = 0
		S%numberOfLights = 0
		S%SHAPE_READ = 0
		S%PATH_READ = 0
		call S%bgcolor%init()
	end subroutine init_sc

	subroutine final_sc(S)
		class(Scene), intent(inout) :: S
		integer :: i
		if ( allocated(S%sphere_t) ) then
			deallocate (S%sphere_t)
		end if
		if ( allocated(S%plane_t) ) then
			deallocate (S%plane_t)
		end if
		if ( allocated(S%material_t) ) then
			deallocate (S%material_t)
		end if
		if ( allocated(S%path_t) ) then
			deallocate (S%path_t)
		end if
		if ( allocated(S%light_t) ) then
			deallocate (S%light_t)
		end if
		do i = 1, S%numberOfPaths
			call S%path_t(i)%final()
		end do
	end subroutine final_sc

	integer function Read(S, filename)
		class(Scene), intent(inout) :: S
		character(len=*), intent(in) :: filename
		character, allocatable :: buffer(:)
		character :: tempString(80)
		integer :: SIZE

		S%filename = filename
		S%parse_t%filename = filename
		open(unit=7, file=TRIM(S%filename), status='old', action='read', form='unformatted')
		inquire(unit=7, size=SIZE)
		SIZE = SIZE - 4               ! Subtract 4 as the file header from the actual size of the file
		rewind(7)
		allocate(buffer(1:SIZE))      ! Allocate buffer to read the complete file
		read(unit=7, end=1003, err=1001) buffer(1:SIZE)

		S%SCENE_READ = .FALSE.
		S%SPHERE_READ = 0
		S%PLANE_READ = 0
		S%MATERIAL_READ = 0
		S%LIGHT_READ = 0
		S%PATH_READ = 0
		S%SHAPE_READ = 0
		S%COUNT = 1
		do while (S%COUNT .LE. SIZE)
			SELECT CASE (buffer(S%COUNT))
				case('!')
					S%COUNT = S%COUNT + 1
					call S%parse_t%readComment (SIZE, S%COUNT, buffer)
				case('S')
					tempString(1:5) = (/ 'S','c','e','n','e' /)
					tempString(6:11) = (/ 'S','p','h','e','r','e' /)
					if (.NOT. S%SCENE_READ .AND. S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call readScene (S, SIZE, buffer)
						S%SCENE_READ = .TRUE.
						allocate (S%sphere_t(S%numberOfSpheres))
						allocate (S%plane_t(S%numberOfPlanes))
!						allocate (Sphere :: S%objects(S%GetNumberOfShapes()))
						allocate (S%material_t(S%numberOfMaterials))
						allocate (S%path_t(S%numberOfPaths))
						allocate (S%light_t(S%numberOfLights))
					else if (S%SCENE_READ .AND. (S%MATERIAL_READ .EQ. S%numberOfMaterials) .AND. (S%SPHERE_READ .LT. S%numberOfSpheres) &
							.AND. S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+5), tempString(6:11))) then
						S%COUNT = S%COUNT + 6
						call readSphere(S, SIZE, buffer, S%SPHERE_READ+1)
						S%SPHERE_READ = S%SPHERE_READ + 1
						S%SHAPE_READ = S%SHAPE_READ + 1
					end if
				case('P')
					tempString(1:5) = (/ 'P','l','a','n','e' /)
					tempString(6:9) = (/ 'P','a','t','h' /)
					if (S%SCENE_READ .AND. (S%MATERIAL_READ .EQ. S%numberOfMaterials) .AND. (S%PLANE_READ .LT. S%numberOfPlanes) &
						.AND. S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call readPlane(S, SIZE, buffer, S%PLANE_READ+1)
						S%PLANE_READ = S%PLANE_READ + 1
					else if (S%SCENE_READ .AND. (S%MATERIAL_READ .EQ. S%numberOfMaterials) .AND. (S%PATH_READ .LT. S%numberOfPaths) &
							.AND. S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+3), tempString(6:9))) then
						S%COUNT = S%COUNT + 4
						call readPath(S, Size, buffer, S%PATH_READ+1)
						S%PATH_READ = S%PATH_READ + 1
						S%SHAPE_READ = S%SHAPE_READ + 1
					end if
				case('L')
					tempString(1:5) = (/ 'L','i','g','h','t' /)
					if (S%SCENE_READ .AND. (S%MATERIAL_READ .EQ. S%numberOfMaterials) .AND. (S%LIGHT_READ .LT. S%numberOfLights) &
						.AND. S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call readLight(S, Size, buffer, S%LIGHT_READ+1)
						S%LIGHT_READ = S%LIGHT_READ + 1
					end if
				case('M')
					tempString(1:8) = (/ 'M','a','t','e','r','i','a','l' /)
					if (S%SCENE_READ .AND. (S%MATERIAL_READ .LT. S%numberOfMaterials) &
						.AND. S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+7), tempString(1:8))) then
						S%COUNT = S%COUNT + 8
						call readMaterial(S, Size, buffer, S%MATERIAL_READ+1)
						S%MATERIAL_READ = S%MATERIAL_READ + 1
					end if
				case DEFAULT
			end SELECT
			S%COUNT = S%COUNT + 1
		end do

		goto 1003
1001	print *,"ERROR: Scen::Read: reading file ",TRIM(S%filename)
1003	close(unit=7)

#ifdef __DEBUG__
			print *,""
			print *,"File ",TRIM(S%filename)
			write (*,'(A I7)') "Number of characters:      ",SIZE
			write (*,'(A I7)') "Number of characters read: ", S%COUNT-1
			write (*,'(A I3 A I3 A I3)') "SHAPES:",S%GetNumberOfShapes()," PALNES:",S%numberOfPlanes," LIGHTS:",S%numberOfLights
			write (*,'(A I3 A I3)') "MATERIALS:",S%numberOfMaterials,"    PATHS:",S%numberOfPaths
			write (*,'(A L3 A I3 A I3)') "SCENE:",S%SCENE_READ," MATERIAL:",S%MATERIAL_READ," SPHERE:",S%SPHERE_READ
			write (*,'(A I3 A I3 A I3)') "PLANE:",S%PLANE_READ,"    LIGHT:",S%LIGHT_READ,"   PATH:",S%PATH_READ
			print *,""
#endif
		if (allocated(buffer)) then
			deallocate(buffer)
		end if

		Read = S%COUNT
	end function Read

	!--------------------------------------------------------
	! Read Scene from the scene file
	!--------------------------------------------------------
	subroutine readScene(S, SIZE, buffer)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: SIZE
		character, intent(in) :: buffer(:)
		character :: tempString(80)
		integer :: START, DONE, digitCount
		double precision :: digit(3)

		START = 0
		DONE = 0
		do while (DONE .EQ. 0 .AND. S%COUNT .LE. SIZE)
			SELECT CASE (buffer(S%COUNT))
				case('!')
					S%COUNT = S%COUNT + 1
					call S%parse_t%readComment (SIZE, S%COUNT, buffer)
				case ('{')
					START = 1
				case ('}')
					if (START .NE. 1) then
						print *,"Syntax Error: Missing { in Component Scene in file ",TRIM(S%filename)
						S%ERROR_NUMBER = 101
					end if
					DONE = 1
				case ('V')
					tempString(1:7) = (/ 'V','e','r','s','i','o','n' /)
					tempString(8:15) = (/ 'V','i','e','w','T','y','p','e' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+6), tempString(1:7))) then
						S%COUNT = S%COUNT + 7
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						S%version = digit(1)
					else if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+7), tempString(8:15))) then
						S%COUNT = S%COUNT + 8
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						S%viewType = digit(1)
					end if
				case ('B')
					tempString(1:7) = (/ 'B','G','C','o','l','o','r' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+6), tempString(1:7))) then
						S%COUNT = S%COUNT + 7
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						S%bgcolor = RGBColor ( digit(1), digit(2), digit(3) )
					end if
				case ('W')
					tempString(1:5) = (/ 'W','i','d','t','h' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						S%width = digit(1)
					end if
				case ('H')
					tempString(1:6) = (/ 'H','e','i','g','h','t' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+5), tempString(1:6))) then
						S%COUNT = S%COUNT + 6
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						S%height = digit(1)
					end if
				case ('Z')
					tempString(1:4) = (/ 'Z','o','o','m' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+3), tempString(1:4))) then
						S%COUNT = S%COUNT + 4
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						S%zoom = digit(1)
					end if
				case ('G')
					tempString(1:5) = (/ 'G','a','m','m','a' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						S%gamma = digit(1)
					end if
				case ('N')
					tempString(1:8)  = (/ 'N','u','m','b','e','r','O','f' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+7), tempString(1:8))) then
						S%COUNT = S%COUNT + 8
						tempString(1:9)  = (/ 'M','a','t','e','r','i','a','l','s' /)
						tempString(10:14) = (/ 'P','a','t','h','s' /)
						tempString(15:21) = (/ 'S','p','h','e','r','e','s' /)
						tempString(22:27) = (/ 'P','l','a','n','e','s' /)
						tempString(28:33) = (/ 'L','i','g','h','t','s' /)
						if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+8), tempString(1:9))) then
							S%COUNT = S%COUNT + 9
							call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
							S%numberOfMaterials = digit(1)
						else if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(10:14))) then
							S%COUNT = S%COUNT + 5
							call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
							S%numberOfPaths = digit(1)
						else if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+6), tempString(15:21))) then
							S%COUNT = S%COUNT + 7
							call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
							S%numberOfSpheres = digit(1)
						else if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+5), tempString(22:27))) then
							S%COUNT = S%COUNT + 6
							call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
							S%numberOfPlanes = digit(1)
						else if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+5), tempString(28:33))) then
							S%COUNT = S%COUNT + 6
							call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
							S%numberOfLights = digit(1)
						end if
					end if
				case DEFAULT
			end SELECT
			S%COUNT = S%COUNT + 1
		end do

		return
	end subroutine readScene

	!--------------------------------------------------------
	! Read Sphere from the scene file
	!--------------------------------------------------------
	subroutine readSphere(S, SIZE, buffer, SPHERE_READ)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: SIZE, SPHERE_READ
		character, intent(in) :: buffer(:)
		character :: tempString(80)
		integer :: START, DONE, digitCount, id
		double precision :: digit(3)

		START = 0
		DONE = 0
		do while (DONE .EQ. 0 .AND. S%COUNT .LE. SIZE)
			SELECT CASE (buffer(S%COUNT))
				case('!')
					S%COUNT = S%COUNT + 1
					call S%parse_t%readComment (SIZE, S%COUNT, buffer)
				case ('{')
					START = 1
				case ('}')
					if (START .NE. 1) then
						print *,"Syntax Error: Missing { in Component Scene in file ",TRIM(S%filename)
						S%ERROR_NUMBER = 101
					end if
					DONE = 1
				case ('C')
					tempString(1:6) = (/ 'C','e','n','t','e','r' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+5), tempString(1:6))) then
						S%COUNT = S%COUNT + 6
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%sphere_t(SPHERE_READ)%SetCenter( Vector(digit(1), digit(2), digit(3)) )
					end if
				case ('R')
					tempString(1:6) = (/ 'R','a','d','i','u','s' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+5), tempString(1:6))) then
						S%COUNT = S%COUNT + 6
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%sphere_t(SPHERE_READ)%SetRadius(digit(1))
					end if
				case ('M')
					tempString(1:11) = (/ 'M','a','t','e','r','i','a','l','.','I','d' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+10), tempString(1:11))) then
						S%COUNT = S%COUNT + 11
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						id = digit(1)
						if (id .GE. S%numberOfMaterials) then
							print *,"Scene::readSphere: Error: Wrong material ID in Component Sphere in file ",TRIM(S%filename)
							S%ERROR_NUMBER = 101
						else
							call S%sphere_t(SPHERE_READ)%SetMaterial(S%material_t(id+1))
						end if
					end if
				case ('P')
					tempString(1:7) = (/ 'P','a','t','h','.','I','d' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+6), tempString(1:7))) then
						S%COUNT = S%COUNT + 7
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						id = digit(1)
						if (id .GE. S%PATH_READ .OR. id .GE. S%numberOfPaths) then
							print *,"Scene::readSphere: Error: Wrong path ID in Component Sphere in file ",TRIM(S%filename)
							S%ERROR_NUMBER = 101
						else
							call S%sphere_t(SPHERE_READ)%SetPath(S%path_t(id+1))
						end if
					end if
				case ('S')
					tempString(1:10) = (/ 'S','t','a','r','t','A','n','g','l','e' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+9), tempString(1:10))) then
						S%COUNT = S%COUNT + 10
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%sphere_t(SPHERE_READ)%SetStartAngle(digit(1))
					end if
				case DEFAULT
			end SELECT
			S%COUNT = S%COUNT + 1
		end do

		return
	end subroutine readSphere

	!--------------------------------------------------------
	! Read Plane from the scene file
	!--------------------------------------------------------
	subroutine readPlane(S, SIZE, buffer, PLANE_READ)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: SIZE, PLANE_READ
		character, intent(in) :: buffer(:)
		character :: tempString(80)
		integer :: START, DONE, digitCount, id
		double precision :: digit(3)

		START = 0
		DONE = 0
		do while (DONE .EQ. 0 .AND. S%COUNT .LE. SIZE)
			SELECT CASE (buffer(S%COUNT))
				case('!')
					S%COUNT = S%COUNT + 1
					call S%parse_t%readComment (SIZE, S%COUNT, buffer)
				case ('{')
					START = 1
				case ('}')
					if (START .NE. 1) then
						print *,"Syntax Error: Missing { in Component Scene in file ",TRIM(S%filename)
						S%ERROR_NUMBER = 101
					end if
					DONE = 1
				case ('P')
					tempString(1:5) = (/ 'P','o','i','n','t' /)
					tempString(6:12) = (/ 'P','a','t','h','.','i','d' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%plane_t(PLANE_READ)%SetPoint( Vector(digit(1), digit(2), digit(3)) )
					else if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+6), tempString(6:12))) then
						S%COUNT = S%COUNT + 7
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						id = digit(1)
						if (id .GE. S%PATH_READ .OR. id .GE. S%numberOfPaths) then
							print *,"Scene::readPlane: Error: Wrong path ID in Component Plane in file ",TRIM(S%filename)
							S%ERROR_NUMBER = 101
						else
							call S%plane_t(PLANE_READ)%SetPath(S%path_t(id+1))
						end if
					end if
				case ('N')
					tempString(1:12) = (/ 'N','o','r','m','a','l','V','e','c','t','o','r' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+11), tempString(1:12))) then
						S%COUNT = S%COUNT + 12
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%plane_t(PLANE_READ)%SetNormalVector( Vector(digit(1), digit(2), digit(3)) )
					end if
				case ('M')
					tempString(1:11) = (/ 'M','a','t','e','r','i','a','l','.','I','d' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+10), tempString(1:11))) then
						S%COUNT = S%COUNT + 11
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						id = digit(1)
						if (id .GE. S%numberOfMaterials) then
							print *,"Scene::readPlane: Error: Wrong material ID in Component Plane in file ",TRIM(S%filename)
							S%ERROR_NUMBER = 101
						else
							call S%plane_t(PLANE_READ)%SetMaterial(S%material_t(id+1))
						end if
					end if
				case DEFAULT
			end SELECT
			S%COUNT = S%COUNT + 1
		end do

		return
	end subroutine readPlane

	!--------------------------------------------------------
	! Read Light from the scene file
	!--------------------------------------------------------
	subroutine readLight(S, SIZE, buffer, LIGHT_READ)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: SIZE, LIGHT_READ
		character, intent(in) :: buffer(:)
		character :: tempString(80)
		integer :: START, DONE, digitCount
		double precision :: digit(3)

		START = 0
		DONE = 0
		do while (DONE .EQ. 0 .AND. S%COUNT .LE. SIZE)
			SELECT CASE (buffer(S%COUNT))
				case('!')
					S%COUNT = S%COUNT + 1
					call S%parse_t%readComment (SIZE, S%COUNT, buffer)
				case ('{')
					START = 1
				case ('}')
					if (START .NE. 1) then
						print *,"Syntax Error: Missing { in Component Scene in file ",TRIM(S%filename)
						S%ERROR_NUMBER = 101
					end if
					DONE = 1
				case ('P')
					tempString(1:8) = (/ 'P','o','s','i','t','i','o','n' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+7), tempString(1:8))) then
						S%COUNT = S%COUNT + 8
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%light_t(LIGHT_READ)%SetPosition( Vector(digit(1), digit(2), digit(3)) )
					end if
				case ('C')
					tempString(1:5) = (/ 'C','o','l','o','r' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%light_t(LIGHT_READ)%SetColor( RGBColor(digit(1), digit(2), digit(3)) )
					end if
				case DEFAULT
			end SELECT
			S%COUNT = S%COUNT + 1
		end do

		return
	end subroutine readLight

	!--------------------------------------------------------
	! Read Material from the scene file
	!--------------------------------------------------------
	subroutine readMaterial(S, SIZE, buffer, MATERIAL_READ)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: SIZE, MATERIAL_READ
		character, intent(in) :: buffer(:)
		character :: tempString(80)
		integer :: START, DONE, digitCount
		double precision :: digit(3)

		START = 0
		DONE = 0
		do while (DONE .EQ. 0 .AND. S%COUNT .LE. SIZE)
			SELECT CASE (buffer(S%COUNT))
				case('!')
					S%COUNT = S%COUNT + 1
					call S%parse_t%readComment (SIZE, S%COUNT, buffer)
				case ('{')
					START = 1
				case ('}')
					if (START .NE. 1) then
						print *,"Syntax Error: Missing { in Component Scene in file ",TRIM(S%filename)
						S%ERROR_NUMBER = 101
					end if
					DONE = 1
				case ('R')
					tempString(1:10) = (/ 'R','e','f','l','e','c','t','i','o','n' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+9), tempString(1:10))) then
						S%COUNT = S%COUNT + 10
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%material_t(MATERIAL_READ)%SetReflection(digit(1))
					end if
				case ('T')
					tempString(1:12) = (/ 'T','r','a','n','s','p','a','r','e','n','c','y' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+11), tempString(1:12))) then
						S%COUNT = S%COUNT + 12
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%material_t(MATERIAL_READ)%SetTransparency(digit(1))
					end if
				case ('C')
					tempString(1:5) = (/ 'C','o','l','o','r' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%material_t(MATERIAL_READ)%SetColor( RGBColor(digit(1), digit(2), digit(3)) )
					end if
				case DEFAULT
			end SELECT
			S%COUNT = S%COUNT + 1
		end do

		return
	end subroutine readMaterial

	!--------------------------------------------------------
	! Read Path from the scene file
	!--------------------------------------------------------
	subroutine readPath(S, SIZE, buffer, PATH_READ)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: SIZE, PATH_READ
		character, intent(in) :: buffer(:)
		character :: tempString(80)
		integer :: START, DONE, digitCount, i
		double precision :: digit(3)

		START = 0
		DONE = 0
		do while (DONE .EQ. 0 .AND. S%COUNT .LE. SIZE)
			SELECT CASE (buffer(S%COUNT))
				case('!')
					S%COUNT = S%COUNT + 1
					call S%parse_t%readComment (SIZE, S%COUNT, buffer)
				case ('{')
					START = 1
				case ('}')
					if (START .NE. 1) then
						print *,"Syntax Error: Missing { in Component Scene in file ",TRIM(S%filename)
						S%ERROR_NUMBER = 101
					end if
					if (S%path_t(PATH_READ)%SetLength() .LT. 0.0) then
						print *,"ERROR: Path::Read: Setting the length"
						S%ERROR_NUMBER = 101
					end if
					DONE = 1
				case ('S')
					tempString(1:4) = (/ 'S','t','e','p' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+3), tempString(1:4))) then
						S%COUNT = S%COUNT + 4
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%path_t(PATH_READ)%SetStep(digit(1))
					end if
				case ('M')
					tempString(1:9) = (/ 'M','a','j','o','r','A','x','i','s' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+8), tempString(1:9))) then
						S%COUNT = S%COUNT + 9
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%path_t(PATH_READ)%SetMajorAxis(digit(1))
					end if
					tempString(1:9) = (/ 'M','i','n','o','r','A','x','i','s' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+8), tempString(1:9))) then
						S%COUNT = S%COUNT + 9
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%path_t(PATH_READ)%SetMinorAxis(digit(1))
					end if
				case ('R')
					tempString(1:12) = (/ 'R','o','t','a','t','i','o','n','A','x','i','s' /)
					tempString(13:21) = (/ 'R','o','t','a','t','i','o','n','s' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+11), tempString(1:12))) then
						S%COUNT = S%COUNT + 12
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						i = digit(1)
						call S%path_t(PATH_READ)%SetRotationAxis(i)
					else if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+8), tempString(13:21))) then
						S%COUNT = S%COUNT + 9
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%path_t(PATH_READ)%SetRotations(digit(1))
					end if
				case ('A')
					tempString(1:5) = (/ 'A','n','g','l','e' /)
					if (S%parse_t%cmpStrings(buffer(S%COUNT:S%COUNT+4), tempString(1:5))) then
						S%COUNT = S%COUNT + 5
						call S%parse_t%readDigits (SIZE, S%COUNT, buffer, digit, digitCount, S%ERROR_NUMBER)
						call S%path_t(PATH_READ)%SetAngle(digit(1))
					end if
				case DEFAULT
			end SELECT
			S%COUNT = S%COUNT + 1
		end do

		return
	end subroutine readPath

	integer function GetVersion(S)
		class(Scene), intent(in) :: S
		GetVersion = S%version
	end function GetVersion

	integer function GetWidth(S)
		class(Scene), intent(in) :: S
		GetWidth = S%width
	end function GetWidth

	integer function GetHeight(S)
		class(Scene), intent(in) :: S
		GetHeight = S%height
	end function GetHeight

	integer function GetViewType(S)
		class(Scene), intent(in) :: S
		GetViewType = S%viewType
	end function GetViewType

	double precision function GetZoom(S)
		class(Scene), intent(in) :: S
		GetZoom = S%zoom
	end function GetZoom

	double precision function GetGamma(S)
		class(Scene), intent(in) :: S
		GetGamma = S%gamma
	end function GetGamma

	type(RGBColor) function GetBGColor(S)
		class(Scene), intent(in) :: S
		GetBGColor = S%bgcolor
	end function GetBGColor

	integer function GetNumberOfShapes(S)
		class(Scene), intent(in) :: S
		GetNumberOfShapes = (S%numberOfSpheres + S%numberOfPlanes)
	end function GetNumberOfShapes

	integer function GetNumberOfSpheres(S)
		class(Scene), intent(in) :: S
		GetNumberOfSpheres = S%numberOfSpheres
	end function GetNumberOfSpheres

	integer function GetNumberOfPaths(S)
		class(Scene), intent(in) :: S
		GetNumberOfPaths = S%numberOfPaths
	end function GetNumberOfPaths

	integer function GetNumberOfPlanes(S)
		class(Scene), intent(in) :: S
		GetNumberOfPlanes = S%numberOfPlanes
	end function GetNumberOfPlanes

	integer function GetNumberOfLights(S)
		class(Scene), intent(in) :: S
		GetNumberOfLights = S%numberOfLights
	end function GetNumberOfLights

	function GetSphere(S, n)
		class(Scene), intent(in) :: S
		integer, intent(in) :: n
		type(Sphere) :: GetSphere
		GetSphere = S%sphere_t(n)
	end function GetSphere

	function GetPlane(S, n)
		class(Scene), intent(in) :: S
		integer, intent(in) :: n
		type(Plane) :: GetPlane
		GetPlane = S%plane_t(n)
	end function GetPlane

	function GetMaterial_SCENE(S, n)
		class(Scene), intent(in) :: S
		integer, intent(in) :: n
		type(Material) :: GetMaterial_SCENE
		GetMaterial_SCENE = S%material_t(n)
	end function GetMaterial_SCENE

	function GetPath_SCENE(S, n)
		class(Scene), intent(in) :: S
		integer, intent(in) :: n
		type(Path) :: GetPath_SCENE
		GetPath_SCENE = S%path_t(n)
	end function GetPath_SCENE

	function GetLight(S, n)
		class(Scene), intent(in) :: S
		integer, intent(in) :: n
		type(Light) :: GetLight
		GetLight = S%light_t(n)
	end function GetLight

	subroutine SetZoom(S, z)
		class(Scene), intent(inout) :: S
		real, intent(in) :: z
		S%zoom = z
	end subroutine SetZoom

	subroutine SetPathDataSphere(S, n, pathD)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: n
		type(PathData), intent(in) :: pathD
		call S%sphere_t(n)%SetPathData(pathD)
	end subroutine SetPathDataSphere

	subroutine SetPathDataPlane(S, n, pathD)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: n
		type(PathData), intent(in) :: pathD
		call S%plane_t(n)%SetPathData(pathD)
	end subroutine SetPathDataPlane

	subroutine SetPositionSphere(S, n, x, y, z)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: n
		double precision, intent(in) :: x, y, z
		call S%sphere_t(n)%SetPosition(x, y, z)
	end subroutine SetPositionSphere

	subroutine SetPositionPlane(S, n, x, y, z)
		class(Scene), intent(inout) :: S
		integer, intent(in) :: n
		double precision, intent(in) :: x, y, z
		call S%plane_t(n)%SetPosition(x, y, z)
	end subroutine SetPositionPlane

	subroutine PrintScene(S)
		class(Scene), intent(in) :: S
		character(len=*), parameter :: FMT_F = '(A F7.2)'
		character(len=*), parameter :: FMT_I = '(A I7)'

		write (*, FMT_I) "Version            = ",S%version
		write (*, FMT_I) "Width              = ",S%width
		write (*, FMT_I) "Height             = ",S%height
		write (*, FMT_I) "ViewType           = ",S%viewType
		write (*, FMT_F) "Zoom               = ",S%zoom
		write (*, FMT_F) "Gamma              = ",S%gamma
		write (*, FMT_F) "BGColor            = ",S%bgcolor%red
		write (*, FMT_F) "                   = ",S%bgcolor%green
		write (*, FMT_F) "                   = ",S%bgcolor%blue
		write (*, FMT_I) "NumberOfMaterials  = ",S%numberOfMaterials
		write (*, FMT_I) "NumberOfPaths      = ",S%numberOfPaths
		write (*, FMT_I) "NumberOfSpheres    = ",S%numberOfSpheres
		write (*, FMT_I) "NumberOfPlanes     = ",S%numberOfPlanes
		write (*, FMT_I) "NumberOfLights     = ",S%numberOfLights
	end subroutine PrintScene

	end module SCENE_
