	module PARSE_

	use DEF_

	implicit none

	!
	! @section DESCRIPTION
	!
	! The Parse Class class.
	!
	!
	type, public :: Parse
		character(LEN=256) :: filename

		contains
			procedure :: readComment
			procedure :: readDigits
			procedure :: cmpStrings
			procedure, private :: stringToReal
			procedure, private :: isDigit
	end type Parse

	contains

	!--------------------------------------------------------
	! Read Digits from the scene file
	! 
	! = 1024 will return 1024
	! = 120, 121.5, 122 will return 120 121 122, 3 numbers in structure Value
	!   with digit(1) = 3 and digit(2) = 120, digit(3) = 121.5 and digit(4) = 122
	! = 4a80 returns an error
	! = 4 80 returns an error
	! 
	!--------------------------------------------------------
	subroutine readDigits(P, SIZE, COUNT, buffer, digit, digitCount, ERROR_NUMBER)
		class(Parse), intent(in) :: P
		integer, intent(in) :: SIZE
		integer, intent(inout) :: COUNT, digitCount
		character, intent(in) :: buffer(:)
		double precision, intent(inout) :: digit(3)
		integer, intent(out) :: ERROR_NUMBER
		character :: tempBuf(128)
		integer :: DONE, digitRead, SIGN

		digitRead = 0
		digitCount = 0
		DONE = 0
		do while (DONE .EQ. 0 .AND. COUNT .LE. SIZE)
			SELECT CASE (buffer(COUNT))
				case (' ')
				case ('=')
					do while (DONE .EQ. 0 .AND. COUNT .LE. SIZE)
						COUNT = COUNT + 1
						SELECT CASE (buffer(COUNT))
							case (' ')
								if (digitRead .GT. 0) then
								else
									digitRead = 0
								end if
							case ('-')
								if ( (SIGN .GT. 0) .OR. (digitRead .GT. 0) ) then
									DONE = 1
									digitCount = 0
								else
									SIGN = SIGN + 1
									digitRead = digitRead + 1
									tempBuf(digitRead) = buffer(COUNT)
								end if
							case ('+')
								if ( (SIGN .GT. 0) .OR. (digitRead .GT. 0) ) then
									DONE = 1
									digitCount = 0
								else
									SIGN = SIGN + 1
									digitRead = digitRead + 1
									tempBuf(digitRead) = buffer(COUNT)
								end if
							case (';')
								digitCount = digitCount + 1
								call P%stringToReal(tempBuf(1:digitRead), digit(digitCount))
								DONE = 1
								digitRead = 0
							case (',')
								if (digitCount .GT. 3) then
									print *,"Scene::readDigits: ",tempBuf(:)
									print *,"More values than expected in file ",P%filename
									digit(1) = 0.0
									digit(2) = 0.0
									digit(3) = 0.0
									ERROR_NUMBER = 101
								else
									digitCount = digitCount + 1
									call P%stringToReal(tempBuf(1:digitRead), digit(digitCount))
									SIGN = 0
									digitRead = 0
								end if
							case DEFAULT
								! Check for a decimal and a digit (0 - 9)
								if (buffer(COUNT) .EQ. '.') then
									digitRead = digitRead + 1
									tempBuf(digitRead) = buffer(COUNT)
								else if (P%isDigit(buffer(COUNT))) then
									digitRead = digitRead + 1
									tempBuf(digitRead) = buffer(COUNT)
								else
									DONE = 1
									digitCount = 0
								end if
						end SELECT
					end do
				case DEFAULT
					DONE = 1
			end SELECT
			COUNT = COUNT + 1
		end do
		return
	end subroutine readDigits

	!--------------------------------------------------------
	! Converts a string into real and pass
	! the converted string in number
	!--------------------------------------------------------
	subroutine stringToReal(P, string, number)
		class(Parse), intent(in) :: P
		character, intent(in) :: string(:)
		double precision, intent(inout) :: number
		integer, parameter :: ASCII_0 = 48
		integer :: place(6)
		integer :: i, n, index, start, sign
		place(1:6) = (/ 1, 10, 100, 1000, 10000, 100000 /)

		start = 1
		sign = 1
		if (string(1) .EQ. '-') then
			start = 2
			sign = -1
		else if (string(1) .EQ. '+') then
			start = 2
		end if
		index = size(string) + 2
		do i = start, size(string)
			if (string(i) .EQ. '.') then
				index = i + 1
			end if
		end do
		number = 0.0
		do i = start, index-2
			n = ichar(string(i)) - ASCII_0
			number = number + n * place(index-i-1)
		end do
		do i = index, size(string)
			n = ichar(string(i)) - ASCII_0
			number = number + n * (1.0 / place(i-index+2))
		end do
		if (sign .EQ. -1) number = -number

		return
	end subroutine stringToReal

	!--------------------------------------------------------
	! Checks if the character passed
	! is a digit (ASCII 0 - 9) or not
	!--------------------------------------------------------
	logical function isDigit(P, ch)
		class(Parse), intent(in) :: P
		character, intent(in) :: ch
		integer, parameter :: ASCII_0 = 48
		integer, parameter :: ASCII_9 = 57
		if (iachar(ch) .GE. ASCII_0 .AND. iachar(ch) .LE. ASCII_9) then
			isDigit = .TRUE.
		else
			isDigit = .FALSE.
		end if
		return
	end function isDigit
		
	!--------------------------------------------------------
	! Read comments from the scene file
	!--------------------------------------------------------
	subroutine readComment(P, SIZE, COUNT, buffer)
		class(Parse), intent(in) :: P
		integer, intent(in) :: SIZE
		integer, intent(inout) :: COUNT
		character, intent(in) :: buffer(:)
		integer :: DONE

		Done = 0
		do while (DONE .EQ. 0 .AND. COUNT .LE. SIZE)
			SELECT CASE (buffer(COUNT))
				case (achar(13))             ! Carriage return
					DONE = 1
				case (achar(10))             ! Line feed
					DONE = 1
				case DEFAULT
			end SELECT
			COUNT = COUNT + 1
		end do

		return
	end subroutine readComment

	!--------------------------------------------------------
	! Compares two strings for equality
	!--------------------------------------------------------
	logical function cmpStrings(P, string1, string2)
		class(Parse), intent(in) :: P
		character, intent(in) :: string1(:)
		character, intent(in) :: string2(:)
		logical :: areEqual = .FALSE.
		integer :: count

		if(size(string1) .NE. size(string2)) then
			areEqual = .FALSE.
		else
			areEqual = .TRUE.
			count = 1
			do while (areEqual .EQV. .TRUE. .AND. count .LE. size(string1))
				if (string1(count) .NE. string2(count)) then
					areEqual = .FALSE.
				end if
				count = count + 1
			end do
		end if

		cmpStrings = areEqual
		return
	end function cmpStrings

	end module PARSE_
