#ifndef _PATH_H_
#define _PATH_H_

#include <math.h>
#include "def.h"

namespace khudi
{

#define ROTATE_X					1
#define ROTATE_Y					2
#define ROTATE_Z					3

/**
 * @section DESCRIPTION
 *
 * The PathData Class.
 * Provides the PathData class to store an ellipse as the path for animation
 * This class conatins the actual data i.e used by the rendering class
 *
 */
class PathData
{
public:
	typedef struct
	{
		double X, Y, Z;
	} Data;
	Data *data;
	int length;

	PathData(int len)
	{
		data = new Data[len];
		length = len;
	}

	~PathData(void)
	{
		if (data)
			delete data;
		data = 0;
	}
};

/**
 * @section DESCRIPTION
 *
 * The Path Class.
 * Provides the Path class to store an ellipse as the path for animation
 *
 */
class Path
{
private:
	double majorAxis, minorAxis, angle, step, STEP;
	int length, rotationAxis;
	double THETA, ROTATIONS;
	PathData *pathD;

public:
	Path(void);
	~Path(void);

	void SetStep(double s);
	void SetMajorAxis(double a);
	void SetMinorAxis(double b);
	void SetRotationAxis(int ra);
	void SetRotations(double r);
	void SetAngle(double theta);

	int GetStep(void);
	double GetMajorAxis(void);
	double GetMinorAxis(void);
	int GetRotationAxis(void);
	double GetRotations(void);
	double GetAngle(void);

	int SetLength(void);
	int GetLength(void);
	PathData* Build(int len, double startAngle, double Xc, double Yc, double Zc);
	void print(void);
};

}

#endif /* _PATH_H_ */
