#include <stdio.h>
#include "path/path.h"

using namespace khudi;

Path::Path(void)
	:	majorAxis(0.0),
		minorAxis(0.0),
		angle(0.0),
		step(1.0),
		rotationAxis(ROTATE_Y),
		length(0),
		THETA(0.0),
		pathD(0)
{
}

Path::~Path(void)
{
}

void Path::SetStep(double s)
{
	step = s;
}

void Path::SetMajorAxis(double a)
{
	majorAxis = a;
}

void Path::SetMinorAxis(double b)
{
	minorAxis = b;
}

void Path::SetRotationAxis(int ra)
{
	rotationAxis = ra;
}

void Path::SetRotations(double r)
{
	ROTATIONS = r;
}

void Path::SetAngle(double theta)
{
	angle = theta;
}

int Path::GetStep(void)
{
	return step;
}

double Path::GetMajorAxis(void)
{
	return majorAxis;
}

double Path::GetMinorAxis(void)
{
	return minorAxis;
}

int Path::GetRotationAxis(void)
{
	return rotationAxis;
}

double Path::GetRotations(void)
{
	return ROTATIONS;
}

double Path::GetAngle(void)
{
	return angle;
}

int Path::SetLength(void)
{
	double a = majorAxis;
	double b = minorAxis;
	//
	// Circumference of an ellipse:
	// PI * sqrt( 2 * (a*a + b*b) )
	// where 'a' and 'b' are major and minor axes radii
	//
	// We compute the circumference first and then
	// the divisions/points on the ellipse circumference
	// that are used to calculate the X, Y and Z points in 3D
	//
	double DIVISIONS = PI * sqrt( 2 * (a*a + b*b) );    // Circumference
	STEP = ( (2 * PI) / DIVISIONS ) * step;
	length = (DIVISIONS / step) + 1.0;
	if (length <= 0)
	{
		fprintf(stdout, "ERROR: Path::SetLength: Setting the length, Length: %d\n", length);
		return -1;
	}
#ifdef __DEBUG__
	fprintf(stdout, "-  step: %9.7f\n", step);
	fprintf(stdout, "-- STEP: %9.7f a: %9.7f b: %9.7f angle: %9.7f length: %d DIVISIONS: %9.7f --- %d %d %d\n", STEP, a, b, angle, length, DIVISIONS, rotationAxis, ROTATE_X, ROTATE_Y);
#endif

	return 0;
}

int Path::GetLength(void)
{
	return length;
}

//
// An ellipse in general position can be expressed parametrically
// as the path of a point (X(t), Y(t)), where
//
// X(t) = Xc + a * cos(t) * cos(theta) - b * sin(t) * sin(theta)
// Y(t) = Yc + a * cos(t) * sin(theta) + b * sin(t) * cos(theta)
// Z(t) = Zc + c * cos(t) * sin(theta) + c * sin(t) * cos(theta)
//
// as the parameter 't' varies from 0 to 2(PI) --> 0 - 360 degrees.
// Here (Xc, Yc) is the center of the ellipse, and 'theta' is the angle
// between the X-axis and the major axis of the ellipse (3D). 'a' and 'b'
// are the major and minor semi axes.
// An ellipse becomes a 3D circle (conic section) when a = b.
//
// Using an eclipse to get points for circle in 3D
//
PathData* Path::Build(int len, double startAngle, double Xc, double Yc, double Zc)
{
	double a = majorAxis;
	double b = minorAxis;
	double X, Y, Z;
	double t = angle;
	t = t * (PI / 180.0);
	startAngle = startAngle * (PI / 180.0);

	//
	// If length of the path is less than the longest path then
	// stretch the length to the longest path for smooth animation
	//
#ifdef __DEBUG__
	printf ("Xc: %9.7f Yc: %9.7f Zc: %9.7f\n", Xc, Yc, Zc);
	printf ("len: %d length: %d STEP: %f ROTATIONS: %f\n", len, length, STEP, ROTATIONS);
#endif
	int lengthL = len;
	if (len > length)
		lengthL = len;
#ifdef __DEBUG__
	printf ("len: %d length: %d STEP: %f ROTATIONS: %f\n", len, length, STEP, ROTATIONS);
#endif

	//
	// Build the path
	//
	lengthL = lengthL * ROTATIONS;
	pathD = new PathData(lengthL);
	THETA = startAngle;
	// Compute theta with respect to the position
	if (rotationAxis == ROTATE_X)
	{
		Xc = Xc - ( a * cos(THETA) );
		Yc = Yc - ( b * cos(t) * sin(THETA) );
		Zc = Zc - ( b * sin(t) * sin(THETA) );
		for (int i = 0; i < pathD->length; i++)
		{
			X = Xc + ( a * cos(THETA) );
			Y = Yc + ( b * cos(t) * sin(THETA) );
			Z = Zc + ( b * sin(t) * sin(THETA) );
			pathD->data[i].X = X;
			pathD->data[i].Y = Y;
			pathD->data[i].Z = Z;
			if (THETA >= 2*PI)
				THETA = 0.0;
			THETA += STEP;
		}
	}
	else if (rotationAxis == ROTATE_Y)
	{
		Xc = Xc - ( a * cos(t) * cos(THETA) );
		Yc = Yc - ( b * sin(THETA) );
		Zc = Zc - ( a * sin(t) * cos(THETA) );
		for (int i = 0; i < pathD->length; i++)
		{
			X = Xc + ( a * cos(t) * cos(THETA) );
			Y = Yc + ( b * sin(THETA) );
			Z = Zc + ( a * sin(t) * cos(THETA) );
			pathD->data[i].X = X;
			pathD->data[i].Y = Y;
			pathD->data[i].Z = Z;
			if (THETA >= 2*PI)
				THETA = 0.0;
			THETA += STEP;
		}
	}
	else if (rotationAxis == ROTATE_Z)
	{
		double c = (a + b) / 2.0;
		Xc = Xc - ( a * cos(t) * cos(THETA) );
		Yc = Yc - ( b * cos(t) * sin(THETA) );
		Zc = Zc - ( c * cos(THETA) * sin(THETA) );
		for (int i = 0; i < pathD->length; i++)
		{
			X = Xc + ( a * cos(t) * cos(THETA) );
			Y = Yc + ( b * cos(t) * sin(THETA) );
			Z = Zc + ( c * cos(THETA) * sin(THETA) );
			pathD->data[i].X = X;
			pathD->data[i].Y = Y;
			pathD->data[i].Z = Z;
			if (THETA >= 2*PI)
				THETA = 0.0;
			THETA += STEP;
		}
	}
	return pathD;
}

void Path::print(void)
{
	fprintf(stdout, "Length of path: %d\n", pathD->length);
	for (int i = 0; i < pathD->length; i++)
	{
		fprintf(stdout, "i: %9d: X: %9.7f Y: %9.7f Z: %9.7f\n", i, pathD->data[i].X, pathD->data[i].Y, pathD->data[i].Z);
	}
}
