#ifndef _RAYTRACER_H_
#define _RAYTRACER_H_

#include <math.h>
#include "def.h"
#include "scene/scene.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The RayTracer Class.
 * Provides the RayTracer class ....
 *
 */
class RayTracer
{
private:
	Scene *scene;
	bool refract(Vector &normal, Vector &dir, double n1, double n2);
	bool reflect(Vector &normal, Vector &dir);
	double reflectance(Vector &normal, Vector &dir, double n1, double n2);
	RGBColor transmission(RGBColor clr, double coef, double dist);

public:
	RayTracer(Scene *scene);
	~RayTracer(void);

	bool RayTrace(Ray &ray, RGBColor &color, double &t, double coef, int depth);
};

}

#endif /* _RAYTRACER_H_ */
