#include "raytracer.h"

using namespace khudi;

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
RayTracer::RayTracer(Scene *scene)
{
	RayTracer::scene = scene;
}

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
RayTracer::~RayTracer(void)
{
}

// ------------------------------------------------------------------------------------------
//
// Check all the objects (spheres and planes) in the scene
//
// ------------------------------------------------------------------------------------------
bool RayTracer::RayTrace(Ray &ray, RGBColor &color, double &t, double coef, int depth)
{
	Shape *objectHit;
	int HIT = 0;

	int numObjects = scene->GetNumberOfShapes();
	for (int i = 0; i < numObjects; i++)
	{
		Shape *shape = (Shape *)scene->objects[i];
		if (shape->Hit(ray, t))
		{
			objectHit = (Shape *)scene->objects[i];
			HIT = 1;
		}
	}

	// No object is hit
	if (HIT == 0)
		return false;

	Material *material = objectHit->GetMaterial();

#ifdef __TESTING__
	color = material->GetColor();
	return true;
#endif

	//
	// Computing reflection for next iteration
	// Will also be used in specular shading
	//
	reflect(ray.hitInfo.Normal, ray.dir);
	Ray reflectedRay = Ray(ray.hitInfo.Position + ray.dir * EPSILON, ray.dir);
	double dot = reflectedRay.dir * ray.dir;

	//
	// Diffuse shading --> Dot product of light ray and normal at the hit/intersection point
	//
	Light *light = scene->GetLights();
	for (int i = 0; i < scene->GetNumberOfLights(); light++,i++)
	{
		// Diffuse shading
		Vector dir = light->GetPosition() - ray.hitInfo.Position;
		// Check if ray and light rays are in opposite direction
		if (ray.hitInfo.Normal * dir <= 0.0)
			continue;
		// We need distance to the light (t), so normalize the Vector 'dir' manually
		t = sqrt(dir * dir);
		if (t <= 0.0)
			continue;
		dir = dir / t;
		Ray lightRay(ray.hitInfo.Position, dir);
		bool inShadow = false;
		// computation of the shadows
		for (int i = 0; i < numObjects; i++)
		{
			Shape *shape = (Shape *)scene->objects[i];
			if (shape->Hit(lightRay, t))
			{
				inShadow = true;
				break;
			}
		}
		if (!inShadow)
		{
			// Lambert calculation
			double lambert = (lightRay.dir * ray.hitInfo.Normal) * coef;
			color = color + light->GetColor() * material->GetColor() * lambert;
			// Compute and add specular shading to color
			if (dot > 0.0)
			{
				double specular = pow(dot, 20) * (1.0 - coef);
				color = color + light->GetColor() * specular;
			}
		}
	}

	// Iterate on the next reflection (computed above for specular shading)
	// with a depth level of "depth"
	double reflection = material->GetReflection();
	if (reflection > 0.0 && depth > 0)
	{
		double t1 = 10000.0;
		RGBColor clr = scene->GetBGColor();
		RayTrace(reflectedRay, clr, t1, (coef * reflection), depth-1);
		color = color + clr * reflection;
	}

	//
	// Iterate on the next refraction with a depth level of "depth"
	//
	if (material->GetTransparency() > 0.0 && depth > 0)
	{
		double refractionIn = REFRACTION_IN;//material->GetRefractionIn();
		double refractionOut = REFRACTION_OUT;//material->GetRefractionOut();
		double refraction = refractionIn / refractionOut;
		double prevDist = ray.dir.z;
		ray.dir.z = DISTANCE;
		//
		// Total internal reflection
		//
		if (refract(ray.hitInfo.Normal, ray.dir, refractionIn, refractionOut))
		{
			double t1 = 10000.0;
			RGBColor clr = scene->GetBGColor();
			Ray refractedRay = Ray(ray.hitInfo.Position + ray.dir * EPSILON, ray.dir);
			if (RayTrace(refractedRay, clr, t1, (coef * refraction), depth-1))
			{
				RayTrace(refractedRay, clr, t1, (coef * refraction), depth-1);
				material = refractedRay.hitInfo.material;
			}
			RGBColor transparency = transmission(material->GetColor(), coef, refractedRay.hitInfo.Distance);
			transparency = transparency * material->GetTransparency();
			if (clr.red > 1.0 || clr.green > 1.0 || clr.blue > 1.0)
			{
				clr = scene->GetBGColor();
			}
			color = color + clr * transparency * coef * 0.5;
		}
		//
		// No total internal reflection
		//
		else
		{
			double t1 = 10000;
			RGBColor clr = scene->GetBGColor();
			Ray refractedRay = Ray(ray.hitInfo.Position + ray.dir * EPSILON, ray.dir);
			if (RayTrace(refractedRay, clr, t1, (coef * refraction), depth-1))
			{
				RayTrace(refractedRay, clr, t1, (coef * refraction), depth-1);
				material = refractedRay.hitInfo.material;
			}
			double refl = reflectance(refractedRay.hitInfo.Normal, refractedRay.dir, refractionIn, refractionOut);
			refl = refl * material->GetTransparency();
			if (clr.red > 1.0 || clr.green > 1.0 || clr.blue > 1.0)
			{
				clr = scene->GetBGColor();
			}
			color = color + clr * refl * coef * 0.5;
		}
		ray.dir.z = prevDist;
	}

	return true;
}

// ------------------------------------------------------------------------------------------
//
// Refraction equation:
//
// r = (n1/n2) * d + [ (n1/n2) * cos(D) - sqrt(1 - sqr(sin(T)) ] * n
// where
//    cos(D) = -(d * n)
//    sqr(sin(T)) = sqr(n1/n2) * [ 1 - sqr(cos(D)) ]
//
// In case of refraction, there�s a condition that limits the range of incoming angles T.
// Outside this range, the refracted direction vector does not exist. Hence, there�s no
// transmission. This is called total internal reflection. The condition is:
// sin(T) > 1.0
//
// ------------------------------------------------------------------------------------------
bool RayTracer::refract(Vector &normal, Vector &dir, double n1, double n2)
{
	double n = n1 / n2;
	double cosD = -(normal * dir);
	double sinT = n * n * (1.0 - cosD * cosD);

	if (sinT > 1.0)
		return false;

	double cosT = sqrt(1.0 - sinT);
	// Fill the new dir vector
	dir = (dir * n) + (normal * (n * cosD - cosT));
	return true;
}

// ------------------------------------------------------------------------------------------
//
// Reflection equation:
//
// r = d + 2 * [cos(D)] * n
// where cos(D) = -(d * n)
//
// ------------------------------------------------------------------------------------------
bool RayTracer::reflect(Vector &normal, Vector &dir)
{
	double cosD = -(normal * dir);
	// Fill the new dir vector
	dir = dir + normal * cosD * 2.0;
	return true;
}

// ------------------------------------------------------------------------------------------
//
// Applying shlick's approximation for reflectance
//
// ------------------------------------------------------------------------------------------
double RayTracer::reflectance(Vector &normal, Vector &dir, double n1, double n2)
{
	double refl = 1.0;
	double r = (n1 - n2) / (n1 + n2);
	r = r * r;
	double cosD = -(normal * dir);
	if (n1 > n2)
	{
		double n = n1 / n2;
		double sinT = n * n * (1.0 - cosD * cosD);
		if (sinT > 1.0)
			return refl;
		cosD = sqrt(1.0 - sinT);
	}
	double x = 1.0 - cosD;
	refl = r + (1.0 - r) * x * x * x * x * x;

	return refl;
}

// ------------------------------------------------------------------------------------------
//
// Aplying Beer�Lambert law
// How much light is absorbed while traveling through a material.
// The law states that there is a logarithmic dependence between the transmission
// (or transmissivity), T, of light through a substance and the product of the
// absorption coefficient of the substance, and the distance the light travels
// through the material (i.e. the path length).
// Equation is:
// color out = color in ( exp (absorbance) )
// where
// absorbance = color * coefficient * path length of the ray
//
// ------------------------------------------------------------------------------------------
RGBColor RayTracer::transmission(RGBColor clr, double coef, double dist)
{
	RGBColor absorbance = clr * coef * -dist;
	RGBColor transmission = RGBColor ( exp(absorbance.red), exp(absorbance.green), exp(absorbance.blue) );
	return transmission;
}
