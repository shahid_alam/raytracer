#include <time.h>
#include "world.h"

using namespace khudi;

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
World::World(void)
{
}

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
World::~World(void)
{
   if (scene)
      delete (scene);
}

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
int World::Build(char *filename)
{
	scene = new Scene(filename);

	if (scene->Read() < 1)
	{
		fprintf(stdout, "\nWorld::Build: Error Reading File %s\n", filename);
		return -1;
	}
	else
	{
#ifdef __DEBUG__
		Sphere *sphere = scene->GetSpheres();
		Plane *plane = scene->GetPlanes();
		Light *light = scene->GetLights();
		scene->Print();
		//
		// Printing all the spheres in the scene
		//
		fprintf (stdout, "\nSpheres:\n");
		for (int i = 0; i < scene->GetNumberOfSpheres(); sphere++,i++)
		{
			fprintf (stdout, " Sphere[%d]:\n", i);
			Vector v = sphere->GetCenter();
			fprintf (stdout, "   Center:        %7.2f, %7.2f, %7.2f\n", v.x, v.y, v.z);
			fprintf (stdout, "   Radius:        %7.2f\n", sphere->GetRadius());
			fprintf (stdout, "   StartAngle:    %7.2f\n", sphere->GetStartAngle());
			Material *m = sphere->GetMaterial();
			fprintf (stdout, "   Material:\n");
			fprintf (stdout, "      Reflection:    %7.2f\n", m->GetReflection());
			fprintf (stdout, "      RefractionIn:  %7.2f\n", m->GetRefractionIn());
			fprintf (stdout, "      RefractionOut: %7.2f\n", m->GetRefractionOut());
			fprintf (stdout, "      Transparency: %7.2f\n", m->GetTransparency());
			RGBColor c = m->GetColor();
			fprintf (stdout, "      Color:         %7.2f, %7.2f, %7.2f\n", c.red, c.green, c.blue);
			if (sphere->HasPath())
			{
				Path *p = sphere->GetPath();
				fprintf (stdout, "   Path:\n");
				fprintf (stdout, "      Step:          %7d\n", p->GetStep());
				fprintf (stdout, "      MajorAxis:     %7.2f\n", p->GetMajorAxis());
				fprintf (stdout, "      MinorAxis:     %7.2f\n", p->GetMinorAxis());
				fprintf (stdout, "      RotationAxis:  %7d\n", p->GetRotationAxis());
				fprintf (stdout, "      Angle:         %7.2f\n", p->GetAngle());
				fprintf (stdout, "      Length:        %7d\n", p->GetLength());
			}
		}
		//
		// Printing all the planes in the scene
		//
		fprintf (stdout, "\nPlanes:\n");
		for (int i = 0; i < scene->GetNumberOfPlanes(); plane++,i++)
		{
			fprintf (stdout, " Plane[%d]:\n", i);
			Vector v = plane->GetPoint();
			fprintf (stdout, "   Point:         %7.2f, %7.2f, %7.2f\n", v.x, v.y, v.z);
			v = plane->GetNormalVector();
			fprintf (stdout, "   Normal Vector: %7.2f, %7.2f, %7.2f\n", v.x, v.y, v.z);
			Material *m = plane->GetMaterial();
			fprintf (stdout, "   Material:\n");
			fprintf (stdout, "      Reflection: %7.2f\n", m->GetReflection());
			fprintf (stdout, "      RefractionIn:  %7.2f\n", m->GetRefractionIn());
			fprintf (stdout, "      RefractionOut: %7.2f\n", m->GetRefractionOut());
			fprintf (stdout, "      Transparency: %7.2f\n", m->GetTransparency());
			RGBColor c = m->GetColor();
			fprintf (stdout, "      Color:      %7.2f, %7.2f, %7.2f\n", c.red, c.green, c.blue);
			if (plane->HasPath())
			{
				Path *p = plane->GetPath();
				fprintf (stdout, "   Path:\n");
				fprintf (stdout, "      Step:          %7d\n", p->GetStep());
				fprintf (stdout, "      MajorAxis:     %7.2f\n", p->GetMajorAxis());
				fprintf (stdout, "      MinorAxis:     %7.2f\n", p->GetMinorAxis());
				fprintf (stdout, "      RotationAxis:  %7d\n", p->GetRotationAxis());
				fprintf (stdout, "      Angle:         %7.2f\n", p->GetAngle());
				fprintf (stdout, "      Length:        %7d\n", p->GetLength());
			}
		}

		//
		// Printing all the lights in the scene
		//
		fprintf (stdout, "\nLights:\n");
		for (int i = 0; i < scene->GetNumberOfLights(); light++,i++)
		{
			fprintf (stdout, " Light[%d]:\n", i);
			Vector v = light->GetPosition();
			fprintf (stdout, "   Position: %7.2f, %7.2f, %7.2f\n", v.x, v.y, v.z);
			RGBColor c = light->GetColor();
			fprintf (stdout, "   Color:    %7.2f, %7.2f, %7.2f\n", c.red, c.green, c.blue);
		}
#endif
		return 0;
	}

	return -1;
}

// ------------------------------------------------------------------------------------------
//
// Build a TGA image file
//
// Perspective viewing produces more realistic images than orthographic viewing
// By default the VIEWING is 0 i.e; orhtographic viewing
// Use VIEWING_TYPE = 1 for perspective viewing
//
//
// ------------------------------------------------------------------------------------------
bool World::RenderScene(char *filename)
{
	RayTracer *rayTracer;
	rayTracer = new RayTracer(scene);
	// 24 bit RGB uncompressed TGA image
	TGA *tga = new TGA (24, 2, scene->GetWidth(), scene->GetHeight());
	double distance = DISTANCE;
	Ray ray;

	int VIEWING_TYPE = scene->GetViewType();
	double ZOOM = 1.0 / scene->GetZoom();
	double INV_GAMMA = 1.0 / scene->GetGamma();
	for (int y = 0; y < scene->GetHeight(); y++)
	{
		for (int x = 0; x < scene->GetWidth(); x++)
		{
			const double coef = 1.0;
			const int depth = 3;
			double t = 10000.0;
			RGBColor color = scene->GetBGColor();
			if (VIEWING_TYPE == ORTHOGRAPHIC)
			{
				ray.origin = Vector(ZOOM * x, ZOOM * y, -distance);
				ray.dir = Vector(0.0, 0.0, distance);
			}
			else if (VIEWING_TYPE == PERSPECTIVE)
			{
				ray.origin = Vector(scene->GetWidth() / 2.0, scene->GetHeight() / 2.0, -distance);
				ray.dir = Vector(ZOOM * (x - scene->GetWidth() / 2.0 + 0.5), ZOOM * (y - scene->GetHeight() / 2.0 + 0.5), distance);
			}
			ray.dir.Normalize();
			rayTracer->RayTrace(ray, color, t, coef, depth);

			//
			// Gamma correction
			// Color = Color ^ (1 / gamma)
			//
			color = color.power(INV_GAMMA);
			color = color * 255.0;
			color.red = min(color.red, 255.0);
			color.green = min(color.green, 255.0);
			color.blue = min(color.blue, 255.0);
			//
			// Fill the TGA image buffer
			//
			tga->SetColor((int)color.red, (int)color.green, (int)color.blue);
#ifdef __TESTING__
			fprintf (stdout, "Color: (%1.17f, %1.17f, %1.17f)\n", color.red, color.green, color.blue);
#endif
		}
	}

	// Write the TGA image buffer to the file
	tga->Write (filename);//, 3, 255);
	delete (tga);
	return true;
}

// ------------------------------------------------------------------------------------------
//
// Build TGA image files for animation
//
// Perspective viewing produces more realistic images than orthographic viewing
// By default the VIEWING is 0 i.e; orhtographic viewing
// Use VIEWING_TYPE = 1 for perspective viewing
//
//
// ------------------------------------------------------------------------------------------
bool World::RenderAnimation(char *dir)
{
	char filename[256];
	int LENGTH = 0;

	//
	// Getting the longest path
	//
	fprintf (stdout, "Getting the longest path\n");
	int numObjects = scene->GetNumberOfPaths();
	Path *p = scene->GetPaths();
	for (int i = 0; i < numObjects; i++)
	{
		// Find the longest path
		if (p[i].GetLength() > LENGTH)
			LENGTH = p[i].GetLength();
	}

	//
	// Building the path(s)
	//
	int LEN = LENGTH;
	fprintf (stdout, "Building the path(s): The longest path length: %9d\n", LENGTH);
	numObjects = scene->GetNumberOfShapes();
	for (int i = 0; i < numObjects; i++)
	{
		Shape *object = (Shape *)scene->objects[i];
		if (object->HasPath())
		{
			Path *path = object->GetPath();
			Vector c = object->GetPosition();
			PathData *pd = path->Build(LENGTH, object->GetStartAngle(), c.x, c.y, c.z);
			object->SetPathData(pd);
#ifdef __DEBUG__
			path->print();
#endif
			// Find the longest path after adjusting during the build
			if (pd->length > LEN)
				LEN = pd->length;
		}
	}
	if (LEN <= 0)
		LEN = 1;
	LENGTH = LEN;

	//
	// Rendering the images for the subsequent positions
	//
	fprintf (stdout, "Rendering the images: Longest path length after adjusting: %9d\n", LENGTH);
	for (int count = 0; count < LENGTH ; count++)
	{
		//
		// TIMING
		//
		clock_t time_start = clock();
		for (int i = 0; i < numObjects; i++)
		{
			Shape *object = (Shape *)scene->objects[i];
			if (object->HasPathData())
			{
				PathData *pd = object->GetPathData();
				if (count < pd->length)
				{
					object->SetPosition(pd->data[count].X, pd->data[count].Y, pd->data[count].Z);
#ifdef __DEBUG__
					fprintf (stdout, "count: %9d Position: (%9.7f %9.7f %9.7f)\n", count, pd->data[count].X, pd->data[count].Y, pd->data[count].Z);
#endif
				}
			}
		}
		sprintf(filename, "%s/test_%d.tga", dir, 10000+count);
		fprintf(stdout, "Rendering to file: %s\n", filename);
		if (!RenderScene(filename))
			fprintf (stdout, "ERROR: World::RenderAnimation: Rendering scene\n");
		//
		// TIMING
		//
		clock_t time_end = clock();
		double time = (double)(time_end - time_start) / CLOCKS_PER_SEC;
		printf("%f\n", (time * 1000.0));
	}

	return true;
}

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
double World::min(double d1, double d2)
{
	return (d1 > d2) ? d2 : d1;
}
