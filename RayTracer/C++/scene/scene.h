#ifndef _SCENE_H_
#define _SCENE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "def.h"
#include "color/RGBColor.h"
#include "light/light.h"
#include "material/material.h"
#include "path/path.h"
#include "shape/sphere.h"
#include "shape/plane.h"
#include "scene/parse.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The Scene Class class.
 * Read the scene description from the file and fills up the scene data structure.
 * Formats of the file read are:
 *
 */
class Scene
{
private:
	int version;
	int width;
	int height;
	int viewType;
	double zoom;
	double gamma;
	RGBColor bgcolor;
	int	numberOfMaterials;
	int	numberOfPaths;
	int	numberOfSpheres;
	int numberOfPlanes;
	int	numberOfLights;

	Sphere *sphere;
	Plane *plane;
	Material *material;
	Path *path;
	Light *light;
	Parse *parse;

	int COUNT, ERROR_NUMBER;
	FILE *file;
	char *filename;
	int SHAPE_READ;
	int SCENE_READ, SPHERE_READ, PLANE_READ, LIGHT_READ, MATERIAL_READ, PATH_READ;

	char* readScene(char *buffer);
	char* readSphere(char *buffer);
	char* readPlane(char *buffer);
	char* readLight(char *buffer);
	char* readMaterial(char *buffer);
	char* readPath(char *buffer);

public:
	int **objects;

	Scene(char * filename);
	~Scene(void);

	int Read(void);
	int GetVersion(void);
	int GetWidth(void);
	int GetHeight(void);
	int GetViewType(void);
	double GetZoom(void);
	void SetZoom(double z);
	double GetGamma(void);
	RGBColor GetBGColor(void);

	int GetNumberOfShapes(void);
	int GetNumberOfSpheres(void);
	int GetNumberOfPaths(void);
	int GetNumberOfPlanes(void);
	int GetNumberOfLights(void);

	Sphere* GetSpheres(void);
	Path* GetPaths(void);
	Plane* GetPlanes(void);
	Light* GetLights(void);

	void Print(void);
};

}

#endif /* _SCENE_H_ */