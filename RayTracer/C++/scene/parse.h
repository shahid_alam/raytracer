#ifndef _PARSE_H_
#define _PARSE_H_

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The Parse Class class.
 *
 */
class Parse
{
public:
	struct digit
	{
		int n;
		double v[3];
	} Digit;

	Parse(void) { };
	~Parse(void) { };

	char* readComment(int COUNT, char *buffer);
	char* readDigits(int COUNT, char *buffer, int ERROR_NUMBER, char *filename);
};

}

#endif /* _PARSE_H_ */