#include "scene/parse.h"

using namespace khudi;

char* Parse::readComment(int COUNT, char *buffer)
{
	char DONE = 0;
	while (1)
	{
		switch (*buffer++)
		{
		case '\n':
			DONE = 1;
			break;
		case '\r':
			DONE = 1;
			break;
		default:
			break;
		}
		COUNT++;
		if (DONE == 1)
			break;
	}

	return buffer;
}

//
// Read digits (number)
// = 1024 will return 1024
// = 120, 121.5, 122 will return 120 121 122, 3 numbers in structure Value
// with Digit.n = 3 and Digit.v[0] = 120, Digit.v[1] = 121.5 and Digit.v[2] = 122
// = 4a80 returns an error
// = 4 80 returns an error
//
char* Parse::readDigits(int COUNT, char *buffer, int ERROR_NUMBER, char *filename)
{
	char DONE = 0, SIGN = 0, DIGIT = 0;
	char tempBuf[1024];

	Digit.n = 0;
	while (1)
	{
		switch (*buffer)
		{
		case ' ':
			break;
		case '=':
			while (1)
			{
				COUNT++;
				buffer++;
				switch (*buffer)
				{
				case ' ':
					if (DIGIT > 0)
						break;
					DIGIT = 0;
					break;
				case '-':
					if (SIGN > 0 || DIGIT > 0)
					{
						DONE = 1;
						Digit.n = 0;
						break;
					}
					SIGN++;
					tempBuf[DIGIT++] = *buffer;
					break;
				case '+':
					if (SIGN > 0 || DIGIT > 0)
					{
						DONE = 1;
						Digit.n = 0;
						break;
					}
					SIGN++;
					tempBuf[DIGIT++] = *buffer;
					break;
				case ';':
					tempBuf[DIGIT] = '\0';
					Digit.v[Digit.n] = atof(tempBuf);
					Digit.n++;
					DONE = 1;
					DIGIT = 0;
					break;
				case ',':
					if (Digit.n > 3)
					{
						fprintf(stdout, "\nScene::readDigits: [%s] More values than expected in file %s\n", tempBuf, filename);
						Digit.v[0] = Digit.v[1] = Digit.v[2] = 0;
						ERROR_NUMBER = 101;
						break;
					}
					tempBuf[DIGIT] = '\0';
					Digit.v[Digit.n] = atof(tempBuf);
					Digit.n++;
					SIGN = 0;
					DIGIT = 0;
					break;
				default:
					// Check for a decimal and a digit (0 - 9)
					if (*buffer != '.' && !isdigit(*buffer))
					{
						DONE = 1;
						Digit.n = 0;
					}
					tempBuf[DIGIT++] = *buffer;
					break;
				}
				if (DONE == 1)
					break;
			}
			break;
		default:
			DONE = 1;
			break;
		}
		COUNT++;
		buffer++;
		if (DONE == 1)
			break;
	}
	if (Digit.n == 0)
	{
		fprintf(stdout, "\nScene::readDigits: [%s] Error reading digits in file %s\n", tempBuf, filename);
		Digit.v[0] = Digit.v[1] = Digit.v[2] = 0;
		ERROR_NUMBER = 101;
	}

	return buffer;
}
