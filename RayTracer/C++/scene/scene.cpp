#include "scene/scene.h"

using namespace khudi;

Scene::Scene(char *filename)
{
	if (filename == NULL)
	{
		fprintf (stderr, "No Scene File - Nothing to read - Quitting\n");
		ERROR_NUMBER = 101;
	}
	else
	{
		file = fopen (filename, "r");
		if (file == NULL)
		{
			fprintf (stderr, "Can't open the file %s\n", filename);
			ERROR_NUMBER = 101;
		}
		else
		{
			Scene::filename = filename;
			ERROR_NUMBER = 0;
			version = 0;
			width = 0;
			height = 0;
			bgcolor = RGBColor(0.0, 0.0, 0.0);
			viewType = 0;
			zoom = 1.0;
			numberOfMaterials = 0;
			numberOfSpheres = 0;
			numberOfPlanes = 0;
			numberOfLights = 0;
			parse = new Parse();
			parse->Digit.n = 0;
			parse->Digit.v[0] = parse->Digit.v[1] = parse->Digit.v[2] = 0;
			SHAPE_READ = 0;
			SCENE_READ = SPHERE_READ = PLANE_READ = LIGHT_READ = MATERIAL_READ = PATH_READ = 0;
		}
	}
}

Scene::~Scene(void)
{
	if (objects)
		delete(objects);
	objects = 0;
	if (parse)
		delete(parse);
	parse = 0;
}

int Scene::Read(void)
{
	COUNT = 0;
	if (ERROR_NUMBER > 0)
		return COUNT;

	int SIZE, temp;
	char *buffer, tempBuf[1024];

	fseek (file, 0L, SEEK_END);
	SIZE = ftell (file);
	rewind (file);
	buffer = (char *)calloc (SIZE + 1, sizeof(char));
	fread (buffer, 1, SIZE, file);
	SIZE = strlen(buffer);
	fclose (file);

	// Read the complete file into buffer
	while (COUNT <= SIZE)
	{
		switch (*buffer)
		{
		case '!':
			COUNT++;
			buffer++;
			buffer = parse->readComment (COUNT, buffer);
			break;
		case 'S':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			if (!strcmp(tempBuf, "Scene") && !SCENE_READ)
			{
				buffer = readScene(buffer);
				SCENE_READ = 1;
				sphere = new Sphere[numberOfSpheres];
				plane = new Plane[numberOfPlanes];
				objects = new int*[GetNumberOfShapes()];
				material = new Material[numberOfMaterials];
				path = new Path[numberOfPaths];
				light = new Light[numberOfLights];
			}
			else if (SCENE_READ && MATERIAL_READ == numberOfMaterials && !strcmp(tempBuf, "Sphere") && SPHERE_READ < numberOfSpheres)
			{
				buffer = readSphere(buffer);
				SPHERE_READ++;
				SHAPE_READ++;
			}
			break;
		case 'P':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			if (SCENE_READ && MATERIAL_READ == numberOfMaterials)
			{
				if (!strcmp(tempBuf, "Path") && PATH_READ < numberOfPaths)
				{
					buffer = readPath(buffer);
					PATH_READ++;
					if (PATH_READ == numberOfPaths)
						path -= numberOfPaths;
				}
				else if (!strcmp(tempBuf, "Plane") && PLANE_READ < numberOfPlanes)
				{
					buffer = readPlane(buffer);
					PLANE_READ++;
					SHAPE_READ++;
				}
			}
			break;
		case 'L':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			if (SCENE_READ && !strcmp(tempBuf, "Light") && LIGHT_READ < numberOfLights)
			{
				buffer = readLight(buffer);
				LIGHT_READ++;
			}
			break;
		case 'M':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			if (SCENE_READ && !strcmp(tempBuf, "Material") && MATERIAL_READ < numberOfMaterials)
			{
				buffer = readMaterial(buffer);
				MATERIAL_READ++;
				if (MATERIAL_READ == numberOfMaterials)
					material -= numberOfMaterials;
			}
			break;
		default:
			COUNT++;
			buffer++;
			break;
		}
		if (ERROR_NUMBER > 0)
			break;
	}

	if (SPHERE_READ == numberOfSpheres)
		sphere -= numberOfSpheres;
	else
		ERROR_NUMBER = 102;
	if (PLANE_READ == numberOfPlanes)
		plane -= numberOfPlanes;
	else
		ERROR_NUMBER = 102;
	if (LIGHT_READ == numberOfLights)
		light -= numberOfLights;
	else
		ERROR_NUMBER = 102;

	if (ERROR_NUMBER > 0)
	{
		fprintf(stdout, "SPHERES     %d, PLANES     %d, LIGHTS     %d, MATERIALS     %d, PATHS     %d\n", numberOfSpheres, numberOfPlanes,	numberOfLights, numberOfMaterials, numberOfPaths);
		fprintf(stdout, "SPHERE_READ %d, PLANE_READ %d, LIGHT_READ %d, MATERIAL_READ %d, PATH_READ %d\n", SPHERE_READ, PLANE_READ, LIGHT_READ, MATERIAL_READ, PATH_READ);
		return -1;
	}

#ifdef __DEBUG__
	fprintf(stdout, "\nFile %s:\n", filename);
	fprintf(stdout, "Number of characters:      %7d\n", SIZE);
	fprintf(stdout, "Number of characters read: %7d\n", COUNT-1);

	fprintf(stdout, "SHAPES: %d, SPHERES     %d, PLANES     %d, LIGHTS     %d, MATERIALS     %d, PATHS     %d\n", GetNumberOfShapes(), numberOfSpheres, numberOfPlanes,	numberOfLights, numberOfMaterials, numberOfPaths);
	fprintf(stdout, "SHAPE_READ %d, SPHERE_READ %d, PLANE_READ %d, LIGHT_READ %d, MATERIAL_READ %d, PATH_READ %d\n", SHAPE_READ, SPHERE_READ, PLANE_READ, LIGHT_READ, MATERIAL_READ, PATH_READ);
#endif

	return COUNT;
}

char* Scene::readScene(char *buffer)
{
	char tempBuf[1024];
	char START = 0, DONE = 0;
	while (1)
	{
		switch (*buffer)
		{
		case '!':
			COUNT++;
			buffer++;
			buffer = parse->readComment (COUNT, buffer);
			break;
		case '}':
			COUNT++;
			buffer++;
			if (START != 1)               // Syntax Error
			{
				fprintf(stdout, "\nSyntax Error: Missing '{' in Component 'Scene' in file %s\n", filename);
				ERROR_NUMBER = 101;
			}
			DONE = 1;
			break;
		case '{':
			COUNT++;
			buffer++;
			START = 1;
			break;
		case 'V':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Version"))
				version = parse->Digit.v[0];
			else if (!strcmp(tempBuf, "ViewType"))
			{
				viewType = parse->Digit.v[0];
				if (viewType < 0 || viewType > VIEWING_TYPES_SUPPORTED)
					viewType = 0;
			}
			break;
		case 'W':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Width"))
				width = parse->Digit.v[0];
			break;
		case 'H':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Height"))
				height = parse->Digit.v[0];
			break;
		case 'B':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "BGColor"))
				bgcolor = RGBColor(parse->Digit.v[0], parse->Digit.v[1], parse->Digit.v[2]);
			break;
		case 'Z':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Zoom"))
			{
				zoom = parse->Digit.v[0];
				if (zoom < 0.0)
				{
					fprintf(stdout, "\nZoom Error: The zoom number should not be less than '0' in Component 'Scene' in file %s\n", filename);
					ERROR_NUMBER = 101;
				}
			}
			break;
		case 'G':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Gamma"))
			{
				gamma = parse->Digit.v[0];
				if (gamma < 0.0)
				{
					fprintf(stdout, "\nZoom Error: The gamma number should not be less than '0' in Component 'Scene' in file %s\n", filename);
					ERROR_NUMBER = 101;
				}
			}
			break;
		case 'N':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "NumberOfMaterials"))
				numberOfMaterials = parse->Digit.v[0];
			else if (!strcmp(tempBuf, "NumberOfPaths"))
				numberOfPaths = parse->Digit.v[0];
			else if (!strcmp(tempBuf, "NumberOfSpheres"))
				numberOfSpheres = parse->Digit.v[0];
			else if (!strcmp(tempBuf, "NumberOfPlanes"))
				numberOfPlanes = parse->Digit.v[0];
			else if (!strcmp(tempBuf, "NumberOfLights"))
				numberOfLights = parse->Digit.v[0];
			break;
		default:
			COUNT++;
			buffer++;
			break;
		}
		if (DONE == 1 || ERROR_NUMBER > 0)
			break;
	}

	return buffer;
}

char* Scene::readSphere(char *buffer)
{
	char tempBuf[1024];
	char N = 0, START = 0, DONE = 0;
	while (1)
	{
		switch (*buffer)
		{
		case '!':

			buffer = parse->readComment (COUNT, buffer);
			break;
		case '}':
			COUNT++;
			buffer++;
			if (START != 1)               // Syntax Error
			{
				fprintf(stdout, "\nScene::readSphere: Syntax Error: Missing '{' in Component 'Sphere' in file %s\n", filename);
				ERROR_NUMBER = 101;
			}
			DONE = 1;
			objects[SHAPE_READ] = (int *)sphere;
			sphere++;
			break;
		case '{':
			COUNT++;
			buffer++;
			START = 1;
			break;
		case 'C':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Center"))
				sphere->SetCenter(Vector(parse->Digit.v[0], parse->Digit.v[1], parse->Digit.v[2]));
			break;
		case 'R':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Radius"))
				sphere->SetRadius(parse->Digit.v[0]);
			break;
		case 'M':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Material.Id"))
			{
				int value = int(parse->Digit.v[0]);
				material += value;
				if (value >= numberOfMaterials)
				{
					fprintf(stdout, "\nScene::readSphere: Error: Wrong material ID in Component 'Sphere' in file %s\n", filename);
					ERROR_NUMBER = 101;
				}
				else
					sphere->SetMaterial(material);
				material -= value;
			}
			break;
		case 'P':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Path.Id"))
			{
				int value = int(parse->Digit.v[0]);
				path += value;
				if (value >= PATH_READ || value >= numberOfPaths)
				{
					fprintf(stdout, "\nScene::readSphere: Error: Wrong path ID in Component 'Sphere' in file %s\n", filename);
					ERROR_NUMBER = 101;
				}
				else
					sphere->SetPath(path);
				path -= value;
			}
			break;
		case 'S':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "StartAngle"))
				sphere->SetStartAngle(parse->Digit.v[0]);
			break;
		default:
			COUNT++;
			buffer++;
			break;
		}
		if (DONE == 1 || ERROR_NUMBER > 0)
			break;
	}

	return buffer;
}

char* Scene::readPlane(char *buffer)
{
	char tempBuf[1024];
	char N = 0, START = 0, DONE = 0;
	while (1)
	{
		switch (*buffer)
		{
		case '!':
			buffer = parse->readComment (COUNT, buffer);
			break;
		case '}':
			COUNT++;
			buffer++;
			if (START != 1)               // Syntax Error
			{
				fprintf(stdout, "\nScene::readPlane: Syntax Error: Missing '{' in Component 'Plane' in file %s\n", filename);
				ERROR_NUMBER = 101;
			}
			DONE = 1;
			objects[SHAPE_READ] = (int *)plane;
			plane++;
			break;
		case '{':
			COUNT++;
			buffer++;
			START = 1;
			break;
		case 'P':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Point"))
				plane->SetPoint(Vector(parse->Digit.v[0], parse->Digit.v[1], parse->Digit.v[2]));
			else if (!strcmp(tempBuf, "Path.Id"))
			{
				int value = int(parse->Digit.v[0]);
				path += value;
				if (value >= PATH_READ || value >= numberOfPaths)
				{
					fprintf(stdout, "\nScene::readPlane: Error: Wrong path ID in Component 'Plane' in file %s\n", filename);
					ERROR_NUMBER = 101;
				}
				else
					plane->SetPath(path);
				path -= value;
			}
			break;
		case 'N':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "NormalVector"))
				plane->SetNormalVector(Vector(parse->Digit.v[0], parse->Digit.v[1], parse->Digit.v[2]));
			break;
		case 'M':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Material.Id"))
			{
				int value = int(parse->Digit.v[0]);
				material += value;
				if (value >= numberOfMaterials)
				{
					fprintf(stdout, "\nScene::readPlane: Error: Wrong material ID in Component 'Plane' in file %s\n", filename);
					ERROR_NUMBER = 101;
				}
				else
					plane->SetMaterial(material);
				material -= value;
			}
			break;
		default:
			COUNT++;
			buffer++;
			break;
		}
		if (DONE == 1 || ERROR_NUMBER > 0)
			break;
	}

	return buffer;
}

char* Scene::readLight(char *buffer)
{
	char tempBuf[1024];
	char N = 0, START = 0, DONE = 0;
	while (1)
	{
		switch (*buffer)
		{
		case '!':
			buffer = parse->readComment (COUNT, buffer);
			break;
		case '}':
			COUNT++;
			buffer++;
			if (START != 1)               // Syntax Error
			{
				fprintf(stdout, "\nSyntax Error: Missing '{' in Component 'Light' in file %s\n", filename);
				ERROR_NUMBER = 101;
			}
			DONE = 1;
			light++;
			break;
		case '{':
			COUNT++;
			buffer++;
			START = 1;
			break;
		case 'P':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Position"))
				light->SetPosition(Vector(parse->Digit.v[0], parse->Digit.v[1], parse->Digit.v[2]));
			break;
		case 'C':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Color"))
				light->SetColor(RGBColor(parse->Digit.v[0], parse->Digit.v[1], parse->Digit.v[2]));
			break;
		default:
			COUNT++;
			buffer++;
			break;
		}
		if (DONE == 1 || ERROR_NUMBER > 0)
			break;
	}

	return buffer;
}

char* Scene::readMaterial(char *buffer)
{
	char tempBuf[1024];
	char N = 0, START = 0, DONE = 0;
	while (1)
	{
		switch (*buffer)
		{
		case '!':
			buffer = parse->readComment (COUNT, buffer);
			break;
		case '}':
			COUNT++;
			buffer++;
			if (START != 1)               // Syntax Error
			{
				fprintf(stdout, "\nSyntax Error: Missing '{' in Component 'Material' in file %s\n", filename);
				ERROR_NUMBER = 101;
			}
			DONE = 1;
			material++;
			break;
		case '{':
			COUNT++;
			buffer++;
			START = 1;
			break;
		case 'R':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Reflection"))
				material->SetReflection(parse->Digit.v[0]);
			else if (!strcmp(tempBuf, "RefractionIn"))
				material->SetRefractionIn(parse->Digit.v[0]);
			else if (!strcmp(tempBuf, "RefractionOut"))
				material->SetRefractionOut(parse->Digit.v[0]);
			break;
		case 'T':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Transparency"))
				material->SetTransparency(parse->Digit.v[0]);
			break;
		case 'C':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Color"))
			{
				RGBColor color = RGBColor(parse->Digit.v[0], parse->Digit.v[1], parse->Digit.v[2]);
				material->SetColor(color);
			}
			break;
		default:
			COUNT++;
			buffer++;
			break;
		}
		if (DONE == 1 || ERROR_NUMBER > 0)
			break;
	}

	return buffer;
}

char* Scene::readPath(char *buffer)
{
	char tempBuf[1024];
	char N = 0, START = 0, DONE = 0;
	while (1)
	{
		switch (*buffer)
		{
		case '!':
			buffer = parse->readComment (COUNT, buffer);
			break;
		case '}':
			COUNT++;
			buffer++;
			if (START != 1)               // Syntax Error
			{
				fprintf(stdout, "\nSyntax Error: Missing '{' in Component 'Path' in file %s\n", filename);
				ERROR_NUMBER = 101;
			}
			if (path->SetLength() < 0)
			{
				fprintf(stdout, "ERROR: Path::Read: Setting the length\n");
				ERROR_NUMBER = 101;
			}
			DONE = 1;
			path++;
			break;
		case '{':
			COUNT++;
			buffer++;
			START = 1;
			break;
		case 'S':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Step"))
				path->SetStep(parse->Digit.v[0]);
			break;
		case 'M':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "MajorAxis"))
				path->SetMajorAxis(parse->Digit.v[0]);
			else if (!strcmp(tempBuf, "MinorAxis"))
				path->SetMinorAxis(parse->Digit.v[0]);
			break;
		case 'R':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Rotations"))
				path->SetRotations(parse->Digit.v[0]);
			else if (!strcmp(tempBuf, "RotationAxis"))
				path->SetRotationAxis(parse->Digit.v[0]);
			break;
		case 'A':
			sscanf(buffer, "%s", tempBuf);
			COUNT += strlen(tempBuf);
			buffer += strlen(tempBuf);
			buffer = parse->readDigits(COUNT, buffer, ERROR_NUMBER, filename);
			if (!strcmp(tempBuf, "Angle"))
				path->SetAngle(parse->Digit.v[0]);
			break;
		default:
			COUNT++;
			buffer++;
			break;
		}
		if (DONE == 1 || ERROR_NUMBER > 0)
			break;
	}

	return buffer;
}

int Scene::GetVersion(void)
{
	return version;
}

int Scene::GetWidth(void)
{
	return width;
}

int Scene::GetHeight(void)
{
	return height;
}

int Scene::GetViewType(void)
{
	return viewType;
}

double Scene::GetZoom(void)
{
	return zoom;
}

double Scene::GetGamma(void)
{
	return gamma;
}

RGBColor Scene::GetBGColor(void)
{
	return bgcolor;
}

int Scene::GetNumberOfShapes(void)
{
	return (numberOfSpheres + numberOfPlanes);
}

int Scene::GetNumberOfSpheres(void)
{
	return numberOfSpheres;
}

int Scene::GetNumberOfPaths(void)
{
	return numberOfPaths;
}

int Scene::GetNumberOfPlanes(void)
{
	return numberOfPlanes;
}

int Scene::GetNumberOfLights(void)
{
	return numberOfLights;
}

Sphere* Scene::GetSpheres(void)
{
	return sphere;
}

Path* Scene::GetPaths(void)
{
	return path;
}

Plane* Scene::GetPlanes(void)
{
	return plane;
}

Light* Scene::GetLights(void)
{
	return light;
}

void Scene::SetZoom(double z)
{
	zoom = z;
}

void Scene::Print(void)
{
	fprintf (stdout, "\nVersion            = %5d\n", version);
	fprintf (stdout, "Width              = %5d\n", width);
	fprintf (stdout, "Height             = %5d\n", height);
	fprintf (stdout, "ViewType           = %5d\n", viewType);
	fprintf (stdout, "Zoom               = %7.2f\n", zoom);
	fprintf (stdout, "Gamma              = %7.2f\n", gamma);
	fprintf (stdout, "BGColor            = %7.2f\n", bgcolor.red);
	fprintf (stdout, "                   = %7.2f\n", bgcolor.green);
	fprintf (stdout, "                   = %7.2f\n", bgcolor.blue);
	fprintf (stdout, "NumberOfMaterials  = %5d\n", numberOfMaterials);
	fprintf (stdout, "NumberOfPaths      = %5d\n", numberOfPaths);
	fprintf (stdout, "NumberOfSpheres    = %5d\n", numberOfSpheres);
	fprintf (stdout, "NumberOfPlanes     = %5d\n", numberOfPlanes);
	fprintf (stdout, "NumberOfLights     = %5d\n", numberOfLights);
}
