#ifndef _VECTOR_H_
#define _VECTOR_H_

#include <math.h>

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * A 3D Vector class for basic trignometry
 *
 */
class Vector
{
public:
	double x, y, z;
	Vector(void);                             // Constructor
	Vector(double xp, double yp, double zp);  // Constructor
	Vector(const Vector &v);                  // Copy Constructor
	~Vector(void);                            // Destructor

	// Operator overloading
	Vector & operator= (const Vector &v);     // Assignment
	Vector operator+ (const double d) const;  // Addition with double
	Vector operator+ (const Vector &v) const; // Addition with Vector
	Vector operator- (const double d) const;  // Subtraction with double
	Vector operator- (const Vector &v) const; // Subtraction with Vector
	Vector operator* (const double d) const;  // Multiplication with double
	Vector operator/ (const double d)  const; // Division with double
	double operator* (const Vector &v) const; // Dot product

	Vector Cross(Vector &v);                  // Cross product
	double Length(void);                      // Length
	void Normalize(void);                     // Normalize to a unit Vector
};

}

#endif /* _VECTOR_H_ */
