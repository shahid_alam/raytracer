#include "math/ray.h"

using namespace khudi;

Ray::Ray(void)
	:	origin(0.0, 0.0, 0.0),
		dir(0.0, 0.0, 0.0)
{
}

Ray::Ray(const Vector &o, const Vector &d)
	:	origin(o),
		dir(d)
{
}

Ray::Ray(const Ray &ray)
	:	origin(ray.origin),
		dir(ray.dir)
{
}

Ray::~Ray(void)
{
}

Ray& Ray::operator= (const Ray &ray)
{
	if (this == &ray)
		return (*this);
		
	origin = ray.origin;
	dir = ray.dir;

	return (*this);	
}
