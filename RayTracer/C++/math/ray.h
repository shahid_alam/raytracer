#ifndef _RAY_H_
#define _RAY_H_

#include "math/vector.h"
#include "material/material.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The HitInfo Class.
 * Provides the HitInfo class ....
 *
 */
class HitInfo
{
public:
	Vector Position;     // Position of hit
	Vector Normal;       // Normal vector at hit point
	Material *material;  // Material of the hit object
	double Distance;     // Distance from hit to screen

	HitInfo(void)
		:	Position(0.0, 0.0, 0.0),
			Normal(0.0, 0.0, 0.0),
			material(),
			Distance(0.0)
	{
	}
	~HitInfo()
	{
	}
};

/**
 * @section DESCRIPTION
 *
 * The Ray Class.
 * Provides the Ray class ....
 *
 */
class Ray
{
public:
	Vector origin;
	Vector dir;

	HitInfo hitInfo;

	Ray(void);
	Ray(const Vector &o, const Vector &d);
	Ray(const Ray &ray);
	~Ray(void);
	Ray& operator= (const Ray &ray);
};

}

#endif /* _RAY_H_ */
