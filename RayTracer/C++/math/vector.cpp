#include "math/vector.h"

using namespace khudi;

Vector::Vector(void)
{
	x = y = z = 0.0;
}

Vector::Vector(double xp, double yp, double zp)
{
	x = xp;
	y = yp;
	z = zp;
}

Vector::Vector(const Vector &v)
{
	x = v.x;
	y = v.y;
	z = v.z;
}

Vector::~Vector(void)
{
}

Vector& Vector::operator= (const Vector &v)
{
	if (this == &v)
		return (*this);

	x = v.x;
	y = v.y;
	z = v.z;
	return (*this);
}

Vector Vector::operator+ (const double d) const
{
	return ( Vector(x + d, y + d, z + d) );
}

Vector Vector::operator+ (const Vector &v) const
{
	return ( Vector(x + v.x, y + v.y, z + v.z) );
}

Vector Vector::operator- (const double d) const
{
	return ( Vector(x - d, y - d, z - d) );
}

Vector Vector::operator- (const Vector &v) const
{
	return ( Vector(x - v.x, y - v.y, z - v.z) );
}

Vector Vector::operator* (const double d) const
{
	return ( Vector(x * d, y * d, z * d) );
}

Vector Vector::operator/ (const double d) const
{
	return ( Vector(x / d, y / d, z / d) );
}

// Dot product
double Vector::operator* (const Vector &v) const
{
	return (x*v.x + y*v.y + z*v.z);
}

Vector Vector::Cross(Vector &v)
{
	return ( Vector(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x) );
}

double Vector::Length(void)
{
	return sqrt( (double)((x * x) + (y * y) + (z * z)) );
}

void Vector::Normalize(void)
{
	double length = sqrt(x*x + y*y + z*z);
	x = x / length;
	y = y / length;
	z = z / length;
}
