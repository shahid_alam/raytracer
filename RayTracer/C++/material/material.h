#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include "color/RGBColor.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The Material Class.
 * Provides the Material class ....
 *
 */
class Material
{
private:
	double gloss;
	double transparency;
	double reflection;
	double refractionIn;
	double refractionOut;
	RGBColor color;

public:
	Material(void);
	~Material(void);

	// specifies the Gloss (or shininess) of the element
	// value must be between 1 (very shiney) and 5 (matt) for a realistic effect 
	double GetGloss(void);
	void SetGloss(double value);

	// defines the transparency of the element. 
	// values must be between 0 (opaque) and 1 (fully transparent);
	double GetTransparency(void);
	void SetTransparency(double value);

	// specifies how much light the element will reflect
	// value must be between 0 (no reflection) to 1 (total reflection/mirror)
	double GetReflection(void);
	void SetReflection(double value);

	// refraction index
	// specifies how the material will bend the light rays
	// value must be between <0,1] (total reflection/mirror)
	double GetRefractionIn(void);
	void SetRefractionIn(double value);
	double GetRefractionOut(void);
	void SetRefractionOut(double value);

	// indicates that the material has a texture and therefore the exact
	// u,v coordinates are to be calculated by the element
	// and passed on in the GetColor function
	virtual bool HasTexture(void);

	// retrieves the actual color of the material
	virtual RGBColor GetColor(void);
	virtual void SetColor(RGBColor &color);
};

}

#endif /* _MATERIAL_H_ */
