#include "material/material.h"

using namespace khudi;

Material::Material(void)
	:	gloss(2.0),
		transparency(0.0),
		reflection(0.0),
		refractionIn(0.0),
		refractionOut(0.0),
		color(0.0, 0.0, 0.0)
{
}

Material::~Material(void)
{
}

double Material::GetGloss(void)
{
	return gloss;
}

void Material::SetGloss(double value)
{
	gloss = value;
}

double Material::GetTransparency(void)
{
	return transparency;
}

void Material::SetTransparency(double value)
{
	transparency = value;
}

double Material::GetReflection(void)
{
	return reflection;
}

void Material::SetReflection(double value)
{
	reflection = value;
}

double Material::GetRefractionIn(void)
{
	return refractionIn;
}

void Material::SetRefractionIn(double value)
{
	refractionIn = value;
}

double Material::GetRefractionOut(void)
{
	return refractionOut;
}

void Material::SetRefractionOut(double value)
{
	refractionOut = value;
}

bool Material::HasTexture(void)
{
	return false;
}

RGBColor Material::GetColor(void)
{
	return color;
}

void Material::SetColor(RGBColor &colorp)
{
	color = colorp;
}
