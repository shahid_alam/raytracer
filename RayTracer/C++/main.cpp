#include <math.h>
#include <time.h>
#include "scene/scene.h"
#include "world.h"

using namespace khudi;

int main(int argc, char* argv[])
{
	if (argc > 2)
	{
		World *world = new World();
		fprintf(stdout, "Scene file: %s\n", argv[1]);
		fprintf(stdout, "Images dir: %s\n", argv[2]);
		if (world->Build(argv[1]) == 0)
		{
#ifdef __NO_ANIMATION__
			char filename[1024];
			sprintf(filename, "%s/test.tga", argv[2]);
			world->RenderScene(filename);
#else
			//
			// TIMING
			//
			clock_t time_start = clock();
			world->RenderAnimation(argv[2]);
			//
			// TIMING
			//
			clock_t time_end = clock();
			double time = (double)(time_end - time_start) / CLOCKS_PER_SEC;
			printf("%f\n", (time * 1000.0));
#endif
			fprintf(stdout, "Finished Rendering images\n");
		}
		delete (world);
	}
	else
		fprintf(stdout, "Error reading the scene file or images dir\n");

	return 0;
}

