#include <stdio.h>
#include "light.h"

using namespace khudi;

Light::Light(void)
{
}

Light::Light(const Vector &pointp, const RGBColor &colorp)
	:	point(pointp),
		color(colorp)
{
}

Light::Light(const Light &light)
	:	point(light.point),
		color(light.color)
{
}

Light::~Light(void)
{
}

void Light::SetPosition(const Vector &v)
{
	point = v;
}

Vector Light::GetPosition(void)
{
	return point;
}

RGBColor Light::GetColor(void)
{
	return color;
}

void Light::SetColor(const RGBColor &colorp)
{
	color = colorp;
}
