#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "color/RGBColor.h"
#include "math/vector.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The Light Class.
 * Provides the Light class ....
 *
 */
class Light
{
private:
	Vector point;
	RGBColor color;

public:
	Light(void);
	Light(const Vector &pointp, const RGBColor &colorp);
	Light(const Light &light);
	~Light(void);

	void SetPosition(const Vector &v);
	Vector GetPosition(void);
	void SetColor(const RGBColor &color);
	RGBColor GetColor(void);
};

}

#endif /*  _LIGHT_H_ */