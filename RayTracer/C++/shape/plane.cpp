#include <stdio.h>
#include "plane.h"

using namespace khudi;

Plane::Plane(void)
	:	Shape(),
		point(Vector(0.0, 0.0, 0.0)),
		normal(Vector(0.0, 0.0, 0.0))
{
}

Plane::Plane(const Vector &pointp, const Vector &normalp)
	:	Shape(),
		point(pointp),
		normal(normalp)
{
	// B default the transparency for plane is set to 0.0
	if (GetMaterial()->GetTransparency() > 0.0)
		GetMaterial()->SetTransparency(0.0);
}

Plane::Plane(const Plane &p)
	:	Shape(p),
		point(p.point),
		normal(p.normal)
{
}

Plane::~Plane(void)
{
}

void Plane::SetPoint(const Vector &v)
{
	point = v;
}

Vector Plane::GetPoint(void)
{
	return point;
}

void Plane::SetNormalVector(const Vector &v)
{
	normal = v;
}

Vector Plane::GetNormalVector(void)
{
	return normal;
}

const void Plane::SetPosition(double X, double Y, double Z)
{
	SetPoint(Vector(X, Y, Z));
}

const Vector Plane::GetPosition(void)
{
	return point;
}

//
// Equation of plane:
// (p - a) . n = 0                          (1)
// where a is a known point that lies on the plane
// and n is the normal to the plane.
// p is a point either on or not on the plane.
// if p is on the plane then
// p is on the plane only if the vector from a to p
// is perpendicular to n.
//
// Ray intersection/hit equation:
// p = o + td                               (2)
//
// Substitute equation 2 into 1
// (o + td - a) . n = 0
// Solving for t:
// t = [ (a - o).n ] / (d.n)
//
const bool Plane::Hit(Ray &ray, double &t)
{
	double d = ray.dir * normal;
	if (d <= 0.0)
		return false;

	d = ((point - ray.origin) * normal) / d;
	if ((d > SHAPE_EPSILON) && (d < t))
	{
		t = d;
		ray.hitInfo.Position = ray.origin + (ray.dir * t);
		ray.hitInfo.Normal = normal;
		ray.hitInfo.material = GetMaterial();
		ray.hitInfo.Distance = t;
		return (true);
	}

	return false;
}
