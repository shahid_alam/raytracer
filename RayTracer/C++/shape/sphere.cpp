#include <stdio.h>
#include "sphere.h"

using namespace khudi;

Sphere::Sphere(void)
	:	Shape(),
		center(Vector(0.0, 0.0, 0.0)),
		radius(0.0)
{
}

Sphere::Sphere(const Vector &centerp, double radiusp)
	:	Shape(),
		center(centerp),
		radius(radiusp)
{
}

Sphere::Sphere(const Sphere &s)
	:	Shape(s),
		center(s.center),
		radius(s.radius)
{
}

Sphere::~Sphere(void)
{
}

void Sphere::SetCenter(const Vector &c)
{
	center = c;
}

Vector Sphere::GetCenter(void)
{
	return center;
}

void Sphere::SetRadius(double r)
{
	radius = r;
}

double Sphere::GetRadius(void)
{
	return radius;
}

const void Sphere::SetPosition(double X, double Y, double Z)
{
	SetCenter(Vector(X, Y, Z));
}

const Vector Sphere::GetPosition(void)
{
	return center;
}

//
// Equation of sphere:
// (x-cx)^2 + (y-cy)^2 + (z-cz)^2 - r^2 = 0
// where x, y and z is any 3D point inside the sphere
// and cx, cy and cy is the center of the sphere
//
// Can be writeen as:
// (p-c) . (p-c) - r^2 = 0                (1)
//
// Ray intersection/hit equation:
// p = o + td                               (2)
//
// Substitute equation 2 into 1
// (o+td-c) dot (o+td-c) - r^2 = 0
//
// Solving above equation:
// d*dt^2 + 2d*(o-c)t +(o-c)^2 - r^2
// at^2 + bt + c = 0   --->  Quadratic equation
// where a = d*d, b = 2d*(o-c) and c = (o-c)^2 - r^2
// t = [ -b +- sqrt(b^2 - 4ac) ] / (2a)
//
// Value of the discriminant
// d = b - 4ac
// decides whether there is an intersection/hit or not
//   d < 0   no intersection
//   d = 0   one intersection
//   d > 0   two intersections
//
const bool Sphere::Hit(Ray &ray, double &t)
{
	Vector dist = ray.origin - center;
	double a = ray.dir * ray.dir;
	double b = ray.dir * dist * 2.0;
	double c = dist * dist - radius * radius;
	double D = b * b - 4.0 * a * c;

	bool isHit = false;
	if (D >= 0.0)
	{
		double t0 = ( -b - sqrt(D) ) / ( 2.0 * a );
		double t1 = ( -b + sqrt(D) ) / ( 2.0 * a );
		if ((t0 > SHAPE_EPSILON) && (t0 < t))
		{
			t = t0;
			isHit = true;
		}
		if ((t1 > SHAPE_EPSILON) && (t1 < t))
		{
			t = t1;
			isHit = true;
		}

		if (isHit)
		{
			ray.hitInfo.Position = ray.origin + (ray.dir * t);
			Vector normal = ray.hitInfo.Position - center;
			double temp = normal * normal;
			if (temp <= 0.0)
				isHit = false;
			else
			{
				temp = 1.0 / sqrt(temp);
				ray.hitInfo.Normal = normal * temp;
				ray.hitInfo.material = GetMaterial();
				ray.hitInfo.Distance = t;
			}
		}
	}

	return isHit;
}
