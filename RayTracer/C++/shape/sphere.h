#ifndef _SPHERE_H_
#define _SPHERE_H_

#include <math.h>
#include "math/vector.h"
#include "math/ray.h"
#include "shape/shape.h"
#include "path/path.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The Sphere Class.
 * Provides the Sphere class ....
 *
 */
class Sphere : public Shape
{
private:
	Vector center;
	double radius;

public:
	Sphere(void);
	Sphere(const Vector &centerp, double radiusp);
	Sphere(const Sphere &s);
	~Sphere(void);

	void SetCenter(const Vector &c);
	Vector GetCenter(void);
	void SetRadius(double r);
	double GetRadius();
	const void SetPosition(double X, double Y, double Z);
	const Vector GetPosition(void);
	const bool Hit(Ray &ray, double &t);
};

}

#endif /*  _SPHERE_H_ */
