#ifndef _PLANE_H_
#define _PLANE_H_

#include "shape/shape.h"
#include "math/vector.h"
#include "math/ray.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The Plane Class.
 * Provides the Plane class ....
 *
 */
class Plane : public Shape
{
private:
	Vector point;
	Vector normal;

public:
	Plane(void);
	Plane(const Vector &pointp, const Vector &normalp);
	Plane(const Plane &p);
	~Plane(void);

	void SetPoint(const Vector &v);
	Vector GetPoint(void);
	void SetNormalVector(const Vector &v);
	Vector GetNormalVector(void);
	const void SetPosition(double X, double Y, double Z);
	const Vector GetPosition(void);
	const bool Hit(Ray &ray, double &t);
};

}

#endif /*  _PLANE_H_ */