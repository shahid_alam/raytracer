#include "shape.h"

using namespace khudi;

Shape::Shape(void)
	:	material(0),
		path(0),
		hasPath(false),
		hasPathData(false),
		angle(0.0),
		pathData(0)
{
}

Shape::Shape(const Shape &shape)
{
	if (shape.material)
		material = shape.material;
	else
		material = 0;
	if (shape.path)
		path = shape.path;
	else
		path = 0;
	if (shape.pathData)
		pathData = shape.pathData;
	else
		pathData = 0;
}

Shape::~Shape(void)
{
	if (material)
		delete material;
	if (path)
		delete path;
	if (pathData)
		delete pathData;
	material = 0;
	path = 0;
	pathData = 0;
}

void Shape::SetMaterial(Material *materialp)
{
	material = materialp;
}

Material* Shape::GetMaterial(void)
{
	return material;
}

void Shape::SetPath(Path *pathp)
{
	path = pathp;
	hasPath = true;
}

void Shape::SetPathData(PathData *pathd)
{
	pathData = pathd;
	hasPathData = true;
}

bool Shape::HasPath(void)
{
	return hasPath;
}

bool Shape::HasPathData(void)
{
	return hasPathData;
}

Path* Shape::GetPath(void)
{
	return path;
}

PathData* Shape::GetPathData(void)
{
	return pathData;
}

void Shape::SetStartAngle(double sangle)
{
	angle = sangle;
}

double Shape::GetStartAngle(void)
{
	return angle;
}
