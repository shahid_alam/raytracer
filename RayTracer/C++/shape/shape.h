#ifndef _SHAPE_H_
#define _SHAPE_H_

#include "def.h"
#include "math/ray.h"
#include "material/material.h"
#include "path/path.h"

namespace khudi
{

#define SHAPE_EPSILON					0.1

/**
 * @section DESCRIPTION
 *
 * The Shape Class.
 * Provides the Shape class ....
 *
 */
class Shape
{
private:
	Material *material;
	Path *path;
	PathData *pathData;
	bool hasPath;
	bool hasPathData;
	double angle;

public:
	Shape(void);
	Shape(const Shape &shape);
	virtual ~Shape(void);

	void SetMaterial(Material *materialp);
	Material* GetMaterial(void);

	void SetPath(Path *pathp);
	Path* GetPath(void);
	void SetPathData(PathData *pathd);
	PathData* GetPathData(void);
	void SetStartAngle(double angle);
	double GetStartAngle(void);
	bool HasPath(void);
	bool HasPathData(void);

	const virtual void SetPosition(double X, double Y, double Z) = 0;
	const virtual Vector GetPosition(void) = 0;
	const virtual bool Hit(Ray &ray, double &t) = 0;
};

}

#endif /* _SHAPE_H_ */
