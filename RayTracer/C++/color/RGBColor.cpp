#include "RGBColor.h"

using namespace khudi;

RGBColor::RGBColor(void)
{
	red = green = blue = 0.0;
}

RGBColor::RGBColor(double r, double g, double b)
{
	red = r;
	green = g;
	blue = b;
}

RGBColor::RGBColor(const RGBColor &rgb)
{
	red = rgb.red;
	green =rgb.green ;
	blue = rgb.blue;
}

RGBColor::~RGBColor(void)
{
}

RGBColor& RGBColor::operator= (const RGBColor &rgb)
{
	if (this == &rgb)
		return (*this);

	red = rgb.red;
	green =rgb.green ;
	blue = rgb.blue;

	return (*this);
}

RGBColor RGBColor::operator+ (const double d)
{
	return ( RGBColor(red + d, green + d, blue + d) );
}

RGBColor RGBColor::operator+ (const RGBColor &rgb)
{
	return ( RGBColor(red + rgb.red, green + rgb.green, blue + rgb.blue) );
}

RGBColor RGBColor::operator- (const double d)
{
	return ( RGBColor(red - d, green - d, blue - d) );
}

RGBColor RGBColor::operator- (const RGBColor &rgb)
{
	return ( RGBColor(red - rgb.red, green - rgb.green, blue - rgb.blue) );
}

RGBColor RGBColor::operator* (const double d)
{
	return ( RGBColor(red * d, green * d, blue * d) );
}

RGBColor RGBColor::operator* (const RGBColor &rgb)
{
	return ( RGBColor(red * rgb.red, green * rgb.green, blue * rgb.blue) );
}

RGBColor RGBColor::operator/ (const double d)
{
	return ( RGBColor(red / d, green / d, blue / d) );
}

bool RGBColor::operator== (const RGBColor &rgb)
{
	return ( red == rgb.red && green == rgb.green && blue == rgb.blue );
}

RGBColor RGBColor::power(double p)
{
	return (RGBColor(pow(red, p), pow(green, p), pow(blue, p)));
}
