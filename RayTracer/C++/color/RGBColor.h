#ifndef _RGBCOLOR_H_
#define _RGBCOLOR_H_

#include <math.h>

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The RGBColor Class.
 * Provides the RGBColor class ....
 *
 */
class RGBColor
{
public:
	double red, green, blue;
	RGBColor(void);
	RGBColor(double r, double g, double b);
	RGBColor(const RGBColor &rgb);
	~RGBColor(void);

	RGBColor& operator= (const RGBColor &rgb);
	RGBColor operator+ (const double d);
	RGBColor operator+ (const RGBColor &rgb);
	RGBColor operator- (const double d);
	RGBColor operator- (const RGBColor &rgb);
	RGBColor operator* (const double d);
	RGBColor operator* (const RGBColor &rgb);
	RGBColor operator/ (const double d);
	bool operator== (const RGBColor &rgb);
	RGBColor power(double p);
};

}

#endif /* _RGBCOLOR_H_ */