#ifndef _WORLD_H_
#define _WORLD_H_

#include "image/tga.h"
#include "scene/scene.h"
#include "trace/raytracer.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The World Class.
 * Provides the World class ....
 *
 */
class World
{
private:
	Scene *scene;
	double min(double d1, double d2);

public:
	World(void);
	~World(void);
	int Build(char *filename);
	bool RenderScene(char *filename);
	bool RenderAnimation(char *dir);
};

}

#endif /* _WORLD_H_ */