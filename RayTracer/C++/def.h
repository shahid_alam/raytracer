#ifndef _DEF_H_
#define _DEF_H_

namespace khudi
{

// --------------------------------------------------------
//
// Viewing types
//
// --------------------------------------------------------
#define VIEWING_TYPES_SUPPORTED		02
// Orthographic viewing
// The origin of the Ray is perpendicular and far away (distance) from the scene
#define ORTHOGRAPHIC				00
// Axis-aligned perspective viewing
// The origin of the Ray is a pin hole (centre point) and far away (distance) from the scene
#define PERSPECTIVE					01

//
// Constants
//
//
// Value of PI to 50 decimal places
// N. J. A. Sloane, A Handbook of Integer Sequences, Academic Press, 1973 (includes this sequence).
// N. J. A. Sloane and Simon Plouffe, The Encyclopedia of Integer Sequences, Academic Press, 1995 (includes this sequence).
//
#define PI							3.14159265358979323846264338327950288419716939937510
#define DISTANCE					1000.0
#define REFRACTION_IN				1.0
#define REFRACTION_OUT				1.25
#define EPSILON						0.15

}

#endif /* _DEF_H_ */