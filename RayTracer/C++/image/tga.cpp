#include "image/tga.h"

using namespace khudi;

TGA::TGA(int bitmap, int type, int ROWSP, int COLSP)
{
	BITMAP  = bitmap;
	TYPE = type;
	ROWS  = ROWSP;
	COLS  = COLSP;
	COUNT = 0;
	// Allocate memory for all the 3 (RGB) colors
	color = new Color[ROWS * COLS];
}

TGA::~TGA(void)
{
	// Deallocate memory for all the colors
	delete color;
}

void TGA::Read(char *filename)
{
	fprintf(stdout, "TGA::Read: Not yet implemented\n");
}

void TGA::Write(char *filename)
{
	FILE *file = fopen(filename, "wb");

	if (file == NULL)
	{
		fprintf(stderr, "Can't open the file %s\n", filename);
	}

	fprintf(file, "%c%c", 0, 0);                                  // Space for TGA header
	fprintf(file, "%c", TYPE);                                    // Type of file format
	fprintf(file, "%c%c%c%c%c", 0, 0, 0, 0, 0);                   // Color map specification = 0
	fprintf(file, "%c%c", 0, 0);                                  // Origin X
	fprintf(file, "%c%c", 0, 0);                                  // Origin Y
	fprintf(file, "%c%c", ROWS&0x00FF, (ROWS & 0xFF00) / 256);    // Number of rows / width of image
	fprintf(file, "%c%c", COLS&0x00FF, (COLS & 0xFF00) / 256);    // Number of cols / height of image
	fprintf(file, "%c", BITMAP);                                  // Bitmap / Depth
	fprintf(file, "%c", 0);                                       // Image decriptor byte - origin in lower left-hand corner
	for (int c = 0; c < ROWS*COLS; c++)                           // Image data field - colors in RGB
		fprintf(file, "%c%c%c", color[c].blue, color[c].green, color[c].red);
	fclose (file);
}

void TGA::SetColor(int r, int g, int b)
{
	if (COUNT < ROWS*COLS)
	{
		color[COUNT].red = r;
		color[COUNT].green = g;
		color[COUNT].blue = b;
		COUNT++;
	}
	else
	{
		fprintf(stderr, "Error writing colors: COUNT: %7d 3*ROWS*COLS: %7d\n", COUNT, ROWS*COLS);
	}
}

int TGA::getline(FILE *file, char line[])
{
	int i = 0;
	while (1)
	{
		line[i] = getc (file);
		if (line[i] == '\n' || line[i] == EOF)
			break;
		i++;
	}
	line[i] = '\0';
	return i;
}
