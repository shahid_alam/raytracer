#ifndef _IMAGE_H_
#define _IMAGE_H_

namespace khudi
{

class Image
{
public:
	virtual void Read(char *filename) = 0;
	virtual void Write(char *filename) = 0;
	virtual void SetColor(int r, int g, int b) = 0;
};

}

#endif /* _IMAGE_H_ */