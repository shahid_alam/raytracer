#ifndef _TGA_H_
#define _TGA_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "image/image.h"

namespace khudi
{

/**
 * @section DESCRIPTION
 *
 * The TGA Class.
 * Provides helping routines to handle TGA image format.
 * TGA Format:
 * ......
 *
 */
class TGA : public Image
{
private:
	struct Color
	{
		int red, green, blue;
	};
	Color *color;
	int BITMAP, TYPE, ROWS, COLS, COUNT;
	int getline(FILE *file, char line[]);

public:
	TGA(int bitmap, int type, int ROWSP, int COLSP);
	~TGA(void);
	void Read(char *filename);
	void Write(char *filename);
	void SetColor(int r, int g, int b);
};

}

#endif /* _TGA_H_ */